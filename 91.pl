#!/usr/bin/perl

## GOALS : 
## 1. download the gnomad annotation tables
## 2. download the revised exac annotation table
## 3. Install CADD v1.4

use DBI;
use Getopt::Std;
my $file = $ARGV[0];
if ($file eq '') {
	# check default cred file
	if (-e '../.Credentials/.credentials') {
		$file = '../.Credentials/.credentials';
	}
	else {
		print "Path to credentials file not provided.\n";
		print " USAGE: perl ApplyUpdates.pl /path/to/.credentials.txt\n";
		exit();
	}
}
# read in config file
my %config;
open CONFIG, $file or die "can't open $file: $!";
while (<CONFIG>) {
    chomp;
    s/#.*//; # Remove comments
    s/^\s+//; # Remove opening whitespace
    s/\s+$//;  # Remove closing whitespace
    next unless length;
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
}
close CONFIG;
$userid = $config{'DBUSER'};
$userpass = $config{'DBPASS'};
$host = $config{'DBHOST'};
$scriptdir = $config{'SCRIPTDIR'};
# disable output buffer
$|++;
# argument to skip some steps if performing initial installation.
$install = 0;
if (defined($ARGV[1]) && $ARGV[1] eq 'install') {
	$install = 1;
}

#########################
## Connect to database ##
######################### 
print "Connecting to database\n";

# get current genome build/database to update
my $dbhgb = DBI->connect("dbi:mysql:NGS-Variants-Admin:$host",$userid,$userpass);
my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
$gbsth->execute();
my @row = $gbsth->fetchrow_array();
$gbsth->finish();
$db = "NGS-Variants".$row[0];
$newbuild = $row[1];
$ucscbuild = $row[0];
$ucscbuild =~ s/^-//;
print "\t=> Using current build database (build ".$row[1].")\n";
$dbh = DBI->connect("dbi:mysql:$db:$host",$userid,$userpass) or die("Could not connect to database with credentials provided in $file");
$dbh->{mysql_auto_reconnect} = 1;
$dbh->{PrintError} = 0;
$dbh->{RaiseError} = 0;



## 1. Download GNOMAD DATA
my @files = qw/hg19_gnomAD_g_2.1.txt.gz hg19_gnomAD_g_2.1.txt.idx.gz hg19_gnomAD_e_2.1.txt.gz hg19_gnomAD_e_2.1.txt.idx.gz/;
my @md5s = qw/hg19_gnomAD_g_2.1.txt.md5 hg19_gnomAD_e_2.1.txt.md5/;
my $url = "https://cloud.biomina.be/index.php/s/tPpFxmumhj4Loi2/download?path=%2F&files=";
# download big files
foreach my $file (@files) {
	print "Downloading $file\n";
	my $cmd = "cd $scriptdir/Annotations/ANNOVAR/humandb/ && wget -c -q -O $file '$url$file'";
	system($cmd) == 0 || die("Could not download $file : $url$file : $?\n");
}
# do checksums 
foreach my $md5file (@md5s) {
	# download
	print "Checking MD5sums for $md5file\n";
	my $cmd = "cd $scriptdir/Annotations/ANNOVAR/humandb/ && wget -c -q -O $md5file '$url$md5file'";
	system($cmd) == 0 || die("Could not download $md5file : $url$md5file : $?\n");
	# check
	$cmd = "cd $scriptdir/Annotations/ANNOVAR/humandb/ && md5sum -c $md5file";
	system($cmd) == 0 || die("MD5 checks failed in $md5file : $?\n");
}
# unpack
foreach my $file (@files) {
	print "Unpacking $file\n";
	my $cmd = "cd $scriptdir/Annotations/ANNOVAR/humandb/ && gunzip $file";
	system($cmd) == 0 || die("Could not unpack $file : $?\n");
}

## 2. Download EXAC_AN DATA
@files = qw/hg19_exac03_AN.txt.gz hg19_exac03_AN.txt.idx.gz/;
@md5s = qw/hg19_exac03_AN.txt.md5/;
$url = "https://cloud.biomina.be/index.php/s/tPpFxmumhj4Loi2/download?path=%2F&files=";
# download big files
foreach my $file (@files) {
	print "Downloading $file\n";
	my $cmd = "cd $scriptdir/Annotations/ANNOVAR/humandb/ && wget -c -q -O $file '$url$file' ";
	system($cmd) == 0 || die("Could not download $file : $url$file : $?\n");
}
# do checksums 
foreach my $md5file (@md5s) {
	# download
	print "Checking MD5sums for $md5file\n";
	my $cmd = "cd $scriptdir/Annotations/ANNOVAR/humandb/ && wget -c -q -O $md5file '$url$md5file'";
	system($cmd) == 0 || die("Could not download $md5file : $url$md5file : $?\n");
	# check
	$cmd = "cd $scriptdir/Annotations/ANNOVAR/humandb/ && md5sum -c $md5file";
	system($cmd) == 0 || die("MD5 checks failed in $md5file : $?\n");
}
# unpack
foreach my $file (@files) {
	print "Unpacking $file\n";
	my $cmd = "cd $scriptdir/Annotations/ANNOVAR/humandb/ && gunzip $file";
	system($cmd) == 0 || die("Could not unpack $file : $?\n");
}
# 3. INSTALL CADD

# install miniconda
print "Installing miniconda\n";
my $cmd = "cd $scriptdir/dependencies && wget -q 'https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh' && bash Miniconda2-latest-Linux-x86_64.sh -p '$scriptdir/dependencies/miniconda2' -u -b";
system($cmd) == 0 || die("Failed to install miniconda2\n");
# install cadd & dependencies (BIG)
print "Installing CADDv1.4\n";
$url = "https://cloud.biomina.be/index.php/s/tPpFxmumhj4Loi2/download?path=%2F&files=CADD_v1.4.tar.gz";
$cmd = "cd $scriptdir/Annotations/CADD && export PATH=$scriptdir/dependencies/miniconda2/bin:\$PATH && wget -q -O CADD_v1.4.tar.gz '$url' && tar xzf CADD_v1.4.tar.gz && cd CADD_v1.4 && ./install.auto.sh" ;
system($cmd) == 0 || die("Failed to install CADD.v1.4\n");
# add path to .credentials.
if (defined($config{'PATH'})) {
	# do sed to add it.
	$cmd = "sed -i 's|^PATH=|PATH=$scriptdir/dependencies/miniconda2/bin:|' $file";
	system($cmd) == 0 || die("Could not add miniconda path to existing PATH entry in $file\n");
}
else {
	# create new entry.
	open OUT, ">>$file";
	print OUT "PATH=$scriptdir/dependencies/miniconda2/bin\n";
	close OUT;
}

