ALTER TABLE `NGS-Variants-hg19`.`Variants` ADD UNIQUE `vid` (`id`)COMMENT '';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_dbnsfp30a` CHANGE  `SIFT_score`  `SIFT_score` DECIMAL( 4, 3 ) NOT NULL COMMENT  '1-score, thresh 0.95';

