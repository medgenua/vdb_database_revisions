ALTER TABLE  `NGS-Variants-hg19`.`Variants` ADD  `Stretch` TINYINT( 1 ) NOT NULL DEFAULT  '0',ADD  `StretchUnit` VARCHAR( 255 ) NOT NULL DEFAULT  '',ADD INDEX (  `Stretch` );
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_Samples` ADD  `StretchLengthA` SMALLINT NOT NULL DEFAULT  '0',ADD  `StretchLengthB` SMALLINT NOT NULL DEFAULT  '0';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_Samples` ADD INDEX (  `StretchLengthA` );
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_Samples` ADD INDEX (  `StretchLengthB` );
