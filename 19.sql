DROP TABLE IF EXISTS `NGS-Variants-hg19`.`GO_Summary`;
CREATE TABLE  `NGS-Variants-hg19`.`GO_Summary` (`goid` VARCHAR( 255 ) NOT NULL ,`name` VARCHAR( 255 ) NOT NULL ,`type` VARCHAR( 55 ) NOT NULL ,`obsolete` INT( 11 ) NOT NULL ,`parentalid` TEXT NOT NULL ,`parentalname` TEXT NOT NULL ,PRIMARY KEY (  `goid` ) ,INDEX (  `name` )) ENGINE = MYISAM ;
