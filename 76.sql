CREATE TABLE `NGS-Variants-hg19`.`Report_Section_CheckBox` ( `cid` int(11) NOT NULL AUTO_INCREMENT, `Title` varchar(255) NOT NULL, `Options` varchar(255) NOT NULL, PRIMARY KEY (`cid`), KEY `Title` (`Title`)) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE  `NGS-Variants-hg19`.`Report_Section_CheckBox` CHANGE  `Options`  `Options` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ;
ALTER TABLE  `NGS-Variants-hg19`.`Report_Sections` ADD  `checkboxes` VARCHAR( 255 ) NOT NULL ;
ALTER TABLE  `NGS-Variants-hg19`.`Report_Section_CheckBox` ADD  `uid` INT NOT NULL ,ADD  `Public` TINYINT( 1 ) NOT NULL DEFAULT  '0',ADD INDEX (  `uid` ) ;
CREATE TABLE `NGS-Variants-hg19`.`Users_x_Report_Sections_CheckBox` ( `uid` int(11) NOT NULL, `rsid` int(11) NOT NULL, `edit` tinyint(1) NOT NULL DEFAULT '0', `full` tinyint(1) NOT NULL DEFAULT '0', PRIMARY KEY (`uid`,`rsid`)) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE  `NGS-Variants-hg19`.`Users_x_Report_Sections_CheckBox` CHANGE  `rsid`  `cid` INT( 11 ) NOT NULL ;
ALTER TABLE   `NGS-Variants-hg19`.`Users` ADD  `cram_notified` TINYINT( 1 ) NOT NULL DEFAULT  '0';
