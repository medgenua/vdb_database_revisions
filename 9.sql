DROP TABLE IF EXISTS `NGS-Variants-hg19`.`Variants_x_MutationTaster`;
CREATE TABLE  `NGS-Variants-hg19`.`Variants_x_MutationTaster` (`aid` INT NOT NULL ,`vid` INT NOT NULL ,`Score` FLOAT( 4, 3 ) NOT NULL ,`Effect` VARCHAR( 35 ) NOT NULL ,`Updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,PRIMARY KEY (  `aid` )) ENGINE = MYISAM ;
ALTER TABLE  `Variants_x_MutationTaster` ADD INDEX (  `vid` );
ALTER TABLE  `Variants_x_MutationTaster` ADD INDEX (  `Score` )
ALTER TABLE  `Variants_x_MutationTaster` ADD INDEX (  `Effect` )
ALTER TABLE  `Variants_x_MutationTaster` CHANGE  `aid`  `aid` INT( 11 ) NOT NULL AUTO_INCREMENT
