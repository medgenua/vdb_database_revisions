ALTER TABLE  `NGS-Variants-hg19`.`Samples_x_Samples` CHANGE  `Relation`  `Relation` TINYINT( 4 ) NOT NULL COMMENT  '1=Same||2=sid1 is parent of sid2||3=siblings||5=custom_group_1||6=custom_group_2'
ALTER TABLE  `NGS-Variants-hg19`.`Samples_x_Samples` DROP PRIMARY KEY ,ADD PRIMARY KEY (  `sid1` ,  `sid2` ,  `Relation` )
