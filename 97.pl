#!/usr/bin/perl

use DBI;
use Getopt::Std;

my $file = $ARGV[0];
if ($file eq '') {

	# check default cred file
	if (-e '../.Credentials/.credentials') {
		$file = '../.Credentials/.credentials';
	}
	else{
		print "Path to credentials file not provided.\n";
		print " USAGE: perl ApplyUpdates.pl /path/to/.credentials.txt\n";
		exit();
	}
}
# read in config file
my %config;
open CONFIG, $file or die "can't open $file: $!";
while (<CONFIG>) {
    chomp;
    s/#.*//; # Remove comments
    s/^\s+//; # Remove opening whitespace
    s/\s+$//;  # Remove closing whitespace
    next unless length;
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
}
close CONFIG;
$userid = $config{'DBUSER'};
$userpass = $config{'DBPASS'};
$host = $config{'DBHOST'};
$datadir = $config{'DATADIR'};
$scriptpass = $config{'SCRIPTPASS'};
$scriptuser = $config{'SCRIPTUSER'};
$scriptdir = $config{'SCRIPTDIR'};
# disable output buffer
$|++;


#########################
## Connect to database ##
#########################
print "Connecting to database\n";

# get current genome build/database to update
my $dbhgb = DBI->connect("dbi:mysql:NGS-Variants-Admin:$host",$userid,$userpass);
my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
$gbsth->execute();
my @row = $gbsth->fetchrow_array();
$gbsth->finish();
$db = "NGS-Variants".$row[0];
$newbuild = $row[1];
$ucscbuild = $row[0];
$ucscbuild =~ s/^-//;
print "\t=> Using current build database (build ".$row[1].")\n";
$dbh = DBI->connect("dbi:mysql:$db:$host",$userid,$userpass) or die("Could not connect to database with credentials provided in $file");
$dbh->{mysql_auto_reconnect} = 1;
$dbh->{PrintError} = 0;
$dbh->{RaiseError} = 0;

##########
### AIM ##
##########
# install the ncbiGene annotation table from ANNOVAR.

# fix discrepancy between refGene and refLink tables
#  - overwrite gene symbols in refLink with values from refGene, based on NM_ids, and vice versa
#  - list genes with symbol discrepancies in database.



## 1. install ncbiGene annotation tables.
print "Download datafiles\n";
my $url = "http://hgdownload.soe.ucsc.edu/goldenPath/hg19/database/ncbiRefSeq.txt.gz";
system("wget -O '/tmp/ncbiRefSeq.txt.gz' '$url'") == 0 or die("Could not download RefSeq file: $url\n");
system("zcat '/tmp/ncbiRefSeq.txt.gz' > '$scriptdir/Annotations/ANNOVAR/$table_prefix"."humandb/$ucscbuild"."_ncbiGene.txt'") == 0 or die("Could not extract RefSeq file\n");
system("rm '/tmp/ncbiRefSeq.txt.gz'");
if (!-f "$scriptdir/Annotations/ANNOVAR/$table_prefix"."humandb/seq/$ucscbuild".".fa") {
	system("mkdir -p '$scriptdir/Annotations/ANNOVAR/$table_prefix"."humandb/seq/'") == 0 or die("Could not create folder for sequences");
	my $cmd = "cd $scriptdir/Annotations/ANNOVAR/; ./annotate_variation.pl -downdb seq -buildver $ucscbuild humandb/seq/";
	system($cmd) == 0 or die("Could not download hg19 fasta sequences");
	# clean up
	system("rm $scriptdir/Annotations/ANNOVAR/$table_prefix"."humandb/seq/chr*fa");
	system("rm $scriptdir/Annotations/ANNOVAR/$table_prefix"."humandb/seq/chromFa.tar.gz");
}
print "Construct mRNA files\n";
my $cmd = "cd '$scriptdir/Annotations/ANNOVAR/' ; perl retrieve_seq_from_fasta.pl --format refGene --outfile humandb/".$ucscbuild."_ncbiGeneMrna.fa --seqfile humandb/seq/$ucscbuild.fa humandb/$ucscbuild"."_ncbiGene.txt";
system($cmd) == 0 or die("Could not create mRNA file\n");
# get latest refLink.
print "Download RefLink\n";
$url = "http://hgdownload.cse.ucsc.edu/goldenPath/hgFixed/database/refLink.txt.gz";
system("wget -O '/tmp/refLink.txt.gz' '$url'") == 0 or die("Could not download refLink file: $url\n");
system("zcat '/tmp/refLink.txt.gz' > '$scriptdir/Annotations/ANNOVAR/$table_prefix"."humandb/$ucscbuild"."_refLink_ncbigene.txt'") == 0 or die("Could not extract refLink file\n");
system("rm '/tmp/refLink.txt.gz'");
# 2. update namings.
my $table_prefix = "";
my $hits = 0;
my %replaced = ();
## update namings (novel symbols)
# read in refLink.
open IN, "$scriptdir/Annotations/ANNOVAR/$table_prefix"."humandb/$ucscbuild"."_refLink_ncbigene.txt" or die("Could not open refLink for reading\n");
my %tr_x_gs = ();
my %gid_x_tr = ();
while (my $line = <IN>) {
	chomp($line);
	my @cols = split(/\t/,$line);
	# col 2 == NM_ without version.
	$tr_x_gs{$cols[2]}{'s'} = $cols[0];
	$tr_x_gs{$cols[2]}{'i'} = $cols[6];
	$gid_x_tr{$cols[6]}{$cols[2]} = 0;
}
close IN;
## update ncbiGene.tmp : will hold names from refLink (new names)
open IN, "$scriptdir/Annotations/ANNOVAR/$table_prefix"."humandb/$ucscbuild"."_ncbiGene.txt" or die("Could not open refGene for reading\n");
open OUT, ">$scriptdir/Annotations/ANNOVAR/$table_prefix"."humandb/$ucscbuild"."_ncbiGene.tmp.txt" or die("Could not open tmp for writing\n");
my $isth = $dbh->prepare("REPLACE INTO `GeneName_Discrepancies_ncbigene` (`id`,`refGene`,`refLink`,`transcript`) VALUES (?,?,?,?)");
my %gene_IDs = (); # which geneID were seen in RefGene (are the human ones).
my %back_replace = (); # to replace in refLink...
while (my $line = <IN>) {
	chomp($line);
	my @cols = split(/\t/,$line);
	# col 1 == NM_with version => strip.
	my $full_nm = $cols[1];
	if ($cols[1] =~ m/(.*)\.\d+/) {
		$cols[1] = $1;
	}
	if (defined($tr_x_gs{$cols[1]})) {
		# mark transcript/geneID as seen
		$gid_x_tr{$tr_x_gs{$cols[1]}{i}}{$cols[1]} = 1;
		$gene_IDs{$tr_x_gs{$cols[1]}{i}}{'n'} = $tr_x_gs{$cols[1]}{s};
		$gene_IDs{$tr_x_gs{$cols[1]}{i}}{'o'} = $cols[12];
		# mismatch?
		if ($tr_x_gs{$cols[1]}{'s'} ne $cols[12]) {
			$hits++; # a mismatch (multiple per symbol due to transcripts.
			# list replaced items for next step
			$replaced{$cols[12]}{s} = $tr_x_gs{$cols[1]}{s};
			$replaced{$cols[12]}{i} = $tr_x_gs{$cols[1]}{i};
			# insert it as a mismatch into database
			$isth->execute($tr_x_gs{$cols[1]}{i},$cols[12],$tr_x_gs{$cols[1]}{s},$cols[1]);
			# store old symbol
			$back_replace{$tr_x_gs{$cols[1]}{i}."|".$cols[1]} = $cols[12];
			# update the gene symbol in refGene.tmp
			$cols[12] = $tr_x_gs{$cols[1]}{s};

		}
	}
	else {
		$isth->execute(-1,$cols[12],-1,$cols[1]);
		#print "Transcript/Gene not in refLink : $cols[1] / $cols[12]\n";
	}
	# print (updated) entry;
	$cols[1] = $full_nm;
	print OUT join("\t",@cols)."\n";
}
close OUT;
## update RefLink.tmp.  : will hold names from ncbiGene (old names).
open IN, "$scriptdir/Annotations/ANNOVAR/$table_prefix"."humandb/$ucscbuild"."_refLink_ncbigene.txt" or die("Could not open refLink for reading\n");
open OUT, ">$scriptdir/Annotations/ANNOVAR/$table_prefix"."humandb/$ucscbuild"."_refLink_ncbigene.tmp.txt" or die("Could not open refLink.tmp for wring \n");
my %new_s = ();
while (my $line = <IN>) {
	chomp($line);
	my @cols = split(/\t/,$line);
	# nm on 2 ; id on 6 and symbol on 0
	$new_s{$cols[6]} = $cols[0];
	# update if seen in refGene
	if (defined($back_replace{$cols[6]."|".$cols[2]})) {
		$cols[0] = $back_replace{$cols[6]."|".$cols[2]};
	}
	print OUT join("\t",@cols)."\n";
}
close IN;
close OUT;


## go over gid_x_tr and check if there are "new transcripts" (value == 0, not seen in refGene)
my %novel = ();
foreach(sort {$a cmp $b} keys(%gid_x_tr)) {
	my $gene_ID = $_;
	next if (!defined($gene_IDs{$gene_ID}));
	my $old_symbol = $gene_IDs{$gene_ID}{'o'};
	my $new_symbol = $gene_IDs{$gene_ID}{'n'};
	# transcripts
	foreach(keys(%{$gid_x_tr{$gene_ID}})) {
		my $nm = $_;
		if ($gid_x_tr{$gene_ID}{$nm} == 0) {
			$isth->execute($gene_ID,$old_symbol,$new_symbol,$nm);
			#print "NOVEL transcript for existing Gene: $gene_IDs{$gene_ID} : $gene_ID : $nm \n";

		}
	}
}
# note : truly novel genes (new geneID) are not picked up here.

## 3. WE USE novel names on NCBI-Gene transcript !
if (!-f "$scriptdir/Annotations/ANNOVAR/$table_prefix"."humandb/$ucscbuild"."_ncbiGene.txt.original") {
	system("mv '$scriptdir/Annotations/ANNOVAR/$table_prefix"."humandb/$ucscbuild"."_ncbiGene.txt' '$scriptdir/Annotations/ANNOVAR/$table_prefix"."humandb/$ucscbuild"."_ncbiGene.txt.original'") == 0 or die("could not move datafile 1\n");
}
system("cp '$scriptdir/Annotations/ANNOVAR/$table_prefix"."humandb/$ucscbuild"."_ncbiGene.tmp.txt' '$scriptdir/Annotations/ANNOVAR/$table_prefix"."humandb/$ucscbuild"."_ncbiGene.txt'") == 0 or die("could not copy datafile 2\n");



$isth->finish();
# read in genepanels & convert to ncbigene.
print "Converting gene panels to NCBI\n";
my $rc = $dbh->selectall_arrayref("SELECT gp.Name, gpg.gid, gpg.Symbol,gp.id,gpg.Comment FROM `GenePanels` gp JOIN `GenePanels_x_Genes` gpg ON  gp.id = gpg.gpid");

my %gp = ();
my %lost = ();
my $gins = $dbh->prepare("REPLACE INTO `GenePanels_x_Genes_ncbigene` (gpid,gid,Symbol,Comment) VALUES (?,?,?,?)");
foreach my $r (@$rc) {

	#my ($gpid,$gid,$symbol,$comments) = split(/,/,$_,4);
	my ($gpName,$gid,$symbol,$gpid,$comment) = @$r;
	if (!defined($new_s{$gid})) {
		#print "Gene Lost for $gpName : $gid : $symbol\n";
		$lost{$gpid}{$gid} = $symbol;
		if ($comment ne '') {
			$lost{$gpid}{$gid} .= " : $comment";
		}
		next;
	}
	if (uc($new_s{$gid}) ne uc($symbol)) {
		#print "Symbol changed for $gpName : $gid : $symbol => $new_s{$gid}\n";
		$symbol = $new_s{$gid};
	}
	# insert into the new table.
	$gins->execute($gpid,$gid,$symbol,$comment);
	$gp{"$symbol|$gid"} .= "$gpName ; ";
}

# construct emails.
my $usth = $dbh->prepare("SELECT LastName, FirstName, email,gp.Name FROM `Users` u JOIN `GenePanels` gp ON gp.Owner = u.id WHERE gp.id = ?");
my %mails = ();
foreach my $gpid (keys(%lost)) {
	$usth->execute($gpid);
	my $r = $usth->fetchrow_arrayref();
	if (!defined($mails{$r->[2]})) {
		$mails{$r->[2]}{'body'} = "Dear ".$r->[1]." ".$r->[0].",\r\n\r\n";
		$mails{$r->[2]}{'body'} .= "VariantDB migrated its gene annotations from refGene to ncbiGene. During this migration, some gene symbols listed in GenePanels could not be mapped. Mapping was performed based on GeneID, which should be a stable identifier for all valid RefSeq genes. Known issues exist with pseudogenes and association loci for which no mapping to transcripts exist. Therefore, we advice to double check the values manually.\r\n\r\n The problematic entries are listed below. Please review and re-add the genes manually : \r\n\r\n";
	}
	$mails{$r->[2]}{'body'} .= $r->[3]." :\r\n";
	foreach my $gid (keys(%{$lost{$gpid}})) {
		 $mails{$r->[2]}{'body'} .= "  - GeneID:$gid : $lost{$gpid}{$gid} \r\n";
	}
	$mails{$r->[2]}{'body'} .= "\r\n\r\n";
}
# 3. ANNOTATIONS
print "Annotate : ncbiGene\n";
## Annotate all variants with gene info.
$cmd = "cd $scriptdir/Annotations/ANNOVAR/; perl LoadVariants.pl -a ncbigene";
system($cmd) == 0 or die("could not annotate variants with ncbiGene data\n");

## annotate gene panels
print "Annotate : GenePanels\n";
$cmd = "cd $scriptdir/Annotations/GenePanels/; perl LoadVariants.pl";
system($cmd) == 0 or die("could not annotate variants with GenePanel data\n");

## annotate ClinVar
print "Annotate : ClinVar\n";
$cmd = "cd $scriptdir/Annotations/ClinVar/; perl LoadVariants.pl";
system($cmd) == 0 or die("could not annotate variants with ClinVar data\n");

# 4. UPDATE ENTRIES IN DB

my $sth = $dbh->prepare("SELECT id,`Table_x_Column`,`Item_Value` FROM `Value_Codes` WHERE `Table_x_Column` LIKE 'Variants_x_ANNOVAR_refgene_%'");
$sth->execute();
my $rc = $sth->fetchall_arrayref();
$sth->finish();
my %refgene = ();
foreach my $r (@$rc) {
	$refgene{$r->[0]}{'c'} = $r->[1];
	$refgene{$r->[0]}{'v'} = $r->[2];
}
$sth = $dbh->prepare("SELECT id,`Table_x_Column`,`Item_Value` FROM `Value_Codes` WHERE `Table_x_Column` LIKE 'Variants_x_ANNOVAR_ncbigene_%'");
$sth->execute();
$rc = $sth->fetchall_arrayref();
$sth->finish();
my %ncbigene = ();
foreach my $r (@$rc) {
	$ncbigene{$r->[0]}{'c'} = $r->[1];
	$ncbigene{$r->[0]}{'v'} = $r->[2];
	$ncbigene{$r->[1]."|".$r->[2]} = $r->[0];
}
$sth = $dbh->prepare("SELECT id,`Table_x_Column`,`Item_Value` FROM `Value_Codes` WHERE `Table_x_Column` LIKE 'Variants_x_ClinVar_%'");
$sth->execute();
$rc = $sth->fetchall_arrayref();
$sth->finish();
my %clinvar = ();
foreach my $r (@$rc) {
	$clinvar{$r->[0]}{'c'} = $r->[1];
	$clinvar{$r->[0]}{'v'} = $r->[2];
	if ($r->[1] =~ m/ncbigene/) {
		$clinvar{$r->[1]."|".$r->[2]} = $r->[0];
	}
}

## rewrite the filter conditions.
my $filters_refgene = $dbh->selectall_arrayref("SELECT fid, FilterSettings FROM `Users_x_FilterSettings`");
my $update_filter = $dbh->prepare("UPDATE `Users_x_FilterSettings` SET `FilterSettings` = ? WHERE fid = ?");
foreach my $filter_row (@$filters_refgene) {
	my @filters = split(/@@@/,$filter_row->[1]);
	my $fid = $filter_row->[0];
	my %filter = ();
	foreach my $f (@filters) {
		my ($k,$v) = split(/\|\|\|/,$f);
		$filter{$k} = $v;
	}
	foreach my $k (keys(%filter)) {
		if ($k =~ m/param(\d+)/ && $filter{$k} eq 'RefSeq_VariantType') {
			my $id = $1;
			my @values = split(/__/,$filter{"argument$id"});
			my $nv = "";
			foreach my $v (@values) {
				my $col = $refgene{$v}{'c'};
				$col =~ s/ANNOVAR_refgene/ANNOVAR_ncbigene/;
				my $val = $refgene{$v}{'v'};
				if (!defined($ncbigene{"$col|$val"})) {
					print "missing : $col => $val\n";
					next;
				}
				$nv .= $ncbigene{"$col|$val"}."__";
			}
			#print "OLD: ".$filter{"argument$id"}."\n";
			#print "NEW: ".$nv."\n";
			$filter{"argument$id"} = $nv;
		}
		elsif ($k =~ m/param(\d+)/ && $filter{$k} eq 'RefSeq_GeneLocation') {
			my $id = $1;
			my @values = split(/__/,$filter{"argument$id"});
			my $nv = "";
			foreach my $v (@values) {
				my $col = $refgene{$v}{'c'};
				$col =~ s/ANNOVAR_refgene/ANNOVAR_ncbigene/;
				my $val = $refgene{$v}{'v'};
				#print "$v : '$col|$val'\n";
				if (!defined($ncbigene{"$col|$val"})) {
					#print "missing : $col => $val\n";
					next;
				}
				$nv .= $ncbigene{"$col|$val"}."__";
			}
			$filter{"argument$id"} = $nv;
		}
		elsif ($k =~ m/category(\d+)/ && $filter{$k} eq 'ClinVar_SNPs') {
			my $id = $1;
			if ($filter{"param$id"} eq 'Classification') {
				# clinvar 'Class' is not variant dependent, not recoded to ncbigene
				next;
			}
			my @values = split(/__/,$filter{"argument$id"});
			my $nv = "";
			foreach my $v (@values) {
				# general, some non-coded criteria.
				if ($v !~ m/^\d+$/ || !defined($clinvar{$v})) {
					print "not valid in clinvar : $v\n";
					$nv .= "${v}__";
					next;
				}
				my $col = $clinvar{$v}{'c'};
				$col =~ s/Variants_x_ClinVar_/Variants_x_ClinVar_ncbigene_/;
				my $val = $clinvar{$v}{'v'};
				#print "$v : '$col|$val'\n";
				if (!defined($clinvar{"$col|$val"})) {
					print "missing : $col => $val\n";
					next;
				}
				$nv .= $clinvar{"$col|$val"}."__";
			}
			$filter{"argument$id"} = $nv;
		}
	}
	# recollapse filter.
	my $string = '';
	foreach my $k (keys(%filter)) {
		my $v = $filter{$k};
		$string .= "$k|||$v@@@";
	}
	$string = substr($string,0,-3);
	$update_filter->execute($string,$fid);
}

foreach my $email (keys(%mails)) {
	print "Sending email to $email\n";
	open OUT, ">/tmp/mail.vdb.txt";
	print OUT "To: $email\n";
	print OUT "BCC: geert.vandeweyer\@uantwerpen.be\n";
	print OUT "subject: VariantDB GenePanel review required\n";
	print OUT "from: no-reply\@variantdb.be\n";
	print OUT "\n";
	print OUT $mails{$email}{'body'};
	close OUT;
	system("sendmail -t < /tmp/mail.vdb.txt");
	system("rm /tmp/mail.vdb.txt");
}
