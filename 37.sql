DELETE FROM  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_ensgene` WHERE VariantType=0;
OPTIMIZE TABLE `NGS-Variants-hg19`.`Variants_x_ANNOVAR_ensgene`;
DELETE FROM  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_refgene` WHERE VariantType=0;
OPTIMIZE TABLE `NGS-Variants-hg19`.`Variants_x_ANNOVAR_refgene`;
DELETE FROM  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_knowngene` WHERE VariantType=0;
OPTIMIZE TABLE `NGS-Variants-hg19`.`Variants_x_ANNOVAR_knowngene`;
