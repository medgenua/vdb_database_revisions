DROP TABLE IF EXISTS `NGS-Variants-hg19`.`Variants_x_ANNOVAR_1000g2014oct_asn`;
CREATE TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_1000g2014oct_eas` (`aid` INT( 11 ) NOT NULL AUTO_INCREMENT , `vid` INT( 11 ) NOT NULL , `AlleleFreq` FLOAT( 3, 2 ) NOT NULL ,PRIMARY KEY (  `vid` ,  `aid` ) ,KEY  `AlleleFreq` (  `AlleleFreq` )) ENGINE = MYISAM DEFAULT CHARSET = latin1 COMMENT =  'Annotation Table'/*!50100 PARTITION BY KEY (vid) PARTITIONS 10 */;
CREATE TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_1000g2014oct_sas` (`aid` INT( 11 ) NOT NULL AUTO_INCREMENT , `vid` INT( 11 ) NOT NULL , `AlleleFreq` FLOAT( 3, 2 ) NOT NULL ,PRIMARY KEY (  `vid` ,  `aid` ) ,KEY  `AlleleFreq` (  `AlleleFreq` )) ENGINE = MYISAM DEFAULT CHARSET = latin1 COMMENT =  'Annotation Table'/*!50100 PARTITION BY KEY (vid) PARTITIONS 10 */;
DELETE FROM `NGS-Variants-hg19`.`Variants_x_ANNOVAR_snp138` WHERE rsID = '-1';
DELETE FROM `NGS-Variants-hg19`.`Variants_x_ANNOVAR_esp6500si_aa` WHERE AlleleFreq < 0;
DELETE FROM `NGS-Variants-hg19`.`Variants_x_ANNOVAR_esp6500si_all` WHERE AlleleFreq < 0;
DELETE FROM `NGS-Variants-hg19`.`Variants_x_ANNOVAR_esp6500si_ea` WHERE AlleleFreq < 0;
