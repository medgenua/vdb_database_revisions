ALTER TABLE  `NGS-Variants-hg19`.`Users_x_FilterSettings` ADD  `Highlight` TINYINT NOT NULL DEFAULT  '0',ADD INDEX (  `Highlight` );
ALTER TABLE  `NGS-Variants-hg19`.`Users_x_Shared_Filters` ADD  `Highlight` TINYINT NOT NULL DEFAULT  '0',ADD INDEX (  `Highlight` );
ALTER TABLE  `NGS-Variants-hg19`.`Users_x_Annotations` ADD  `Highlight` TINYINT NOT NULL DEFAULT  '0',ADD INDEX (  `Highlight` );
ALTER TABLE  `NGS-Variants-hg19`.`Users_x_Shared_Annotations` ADD  `Highlight` TINYINT NOT NULL DEFAULT  '0',ADD INDEX (  `Highlight` );
