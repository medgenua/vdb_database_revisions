#!/usr/bin/perl

################
# LOAD MODULES #
################
use DBI;
use Getopt::Std;

my $file = $ARGV[0];
if ($file eq '') {
    # check default cred file
    if (-e '../.Credentials/.credentials') {
        $file = '../.Credentials/.credentials';
    }
    else {
	print "Path to credentials file not provided.\n";
	print " USAGE: perl ApplyUpdates.pl /path/to/.credentials.txt\n";
        exit();
    }
}

# read in config file
my %config;
open CONFIG, $file or die "can't open $file: $!";
while (<CONFIG>) {
    chomp;
    s/#.*//; # Remove comments
    s/^\s+//; # Remove opening whitespace
    s/\s+$//;  # Remove closing whitespace
    next unless length;
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
}
close CONFIG;
my $userid = $config{'DBUSER'};
my $userpass = $config{'DBPASS'};
my $host = $config{'DBHOST'};

# argument to skip some steps if performing initial installation.
$install = 0;
if (defined($ARGV[1]) && $ARGV[1] eq 'install') {
	$install = 1;
	# if installing to this step, then cloned from bitbucket.
	exit;
}


#########################
## Connect to database ##
######################### 
print "Connecting to database\n";

# get current genome build/database to update
my $dbhgb = DBI->connect("dbi:mysql:NGS-Variants-Admin:$host",$userid,$userpass);
my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
$gbsth->execute();
my @row = $gbsth->fetchrow_array();
$gbsth->finish();
$db = "NGS-Variants".$row[0];
$newbuild = $row[1];
$ucscbuild = $row[0];
$ucscbuild =~ s/^-//;
print "\t=> Using current build database (build ".$row[1].")\n";
$dbh = DBI->connect("dbi:mysql:$db:$host",$userid,$userpass) or die("Could not connect to database with credentials provided in $file");
$dbh->{mysql_auto_reconnect} = 1;
$dbh->{PrintError} = 0;
$dbh->{RaiseError} = 0;

# AIMs
#  1. notify admin users of migration ot bitbucket
#  2. provide them with instructions on how to perform the migration.

my $sth =$dbh->prepare("SELECT email FROM `Users` WHERE level = 3");
$sth->execute();

my $result = $sth->fetchall_arrayref();
$sth->finish();

my $admins;
foreach my $line (@$result) {
    my $admin = $line->[0];
    $admins .= "$admin,";
}

$admins = substr($admins,0,-1);
my $mailfile = `mktemp`;

open OUT, ">$mailfile";
print OUT "BCC: geert.vandeweyer\@uantwerpen.be\n";
print OUT "To: $admins\n";
print OUT "subject: VariantDB source changed\n";
print OUT "from: geert.vandeweyer\@uantwerpen.be\n\n";
print OUT "Dear,\r\n\r\n";
print OUT "You received this e-mail because you are the administrator of a local VariantDB installation. The code base was changed successfully to BitBucket.\r\n";
print OUT "Kind regards,\r\nGeert\r\n\r\n";
close OUT;
## send mail
system("sendmail -t < $mailfile");
system("rm $mailfile");
