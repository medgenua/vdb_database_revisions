#!/usr/bin/perl

use DBI;
use Getopt::Std;

my $file = $ARGV[0];
if ($file eq '') {

	# check default cred file
	if (-e '../.Credentials/.credentials') {
		$file = '../.Credentials/.credentials';
	}
	else{
		print "Path to credentials file not provided.\n";
		print " USAGE: perl ApplyUpdates.pl /path/to/.credentials.txt\n";
		exit();
	}
}
# read in config file
my %config;
open CONFIG, $file or die "can't open $file: $!";
while (<CONFIG>) {
    chomp;
    s/#.*//; # Remove comments
    s/^\s+//; # Remove opening whitespace
    s/\s+$//;  # Remove closing whitespace
    next unless length;
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
}
close CONFIG;
$userid = $config{'DBUSER'};
$userpass = $config{'DBPASS'};
$host = $config{'DBHOST'};
$datadir = $config{'DATADIR'};
$scriptpass = $config{'SCRIPTPASS'};
$scriptuser = $config{'SCRIPTUSER'};
$scriptdir = $config{'SCRIPTDIR'};
# disable output buffer
$|++;

# argument to skip some steps if performing initial installation.
$install = 0;
if (defined($ARGV[1]) && $ARGV[1] eq 'install') {
	$install = 1;
}



## GOAL : INSTALL R 3.2.1 + kinship2 library + set pedigree folder permissions (first update web!)

# download+unpack sources.
mkdir("/tmp/R.src");
system("cd /tmp/R.src ; wget 'http://cran.r-project.org/src/base/R-3/R-3.2.1.tar.gz' >/dev/null 2>&1; tar xzf R-3.2.1.tar.gz ; rm R-3.2.1.tar.gz");

# compile
system("cd /tmp/R.src/R-3.2.1 ; NRCPU=`nproc`; ./configure --prefix=$scriptdir/R-3.2.1 --with-cairo --with-libpng --with-x=no; make -j$NRCPU ; make install");

# clean up.
system("rm -Rf /tmp/R.src");

# install extra packages
system("$scriptdir/R-3.2.1/bin/R -q -e \"source('http://bioconductor.org/biocLite.R');biocLite(c('quadprog','kinship2'),ask=F,suppressUpdates=T)\"");


# set permissions
if (!-d "$scriptdir/Images/pedigrees/") {
	system("mkdir '$scriptdir/Images/pedigrees'");
}
system("chmod 777 $scriptdir/Images/pedigrees");
