#!/usr/bin/perl

use DBI;
use Getopt::Std;

my $file = $ARGV[0];
if ($file eq '') {

	# check default cred file
	if (-e '../.Credentials/.credentials') {
		$file = '../.Credentials/.credentials';
	}
	else{
		print "Path to credentials file not provided.\n";
		print " USAGE: perl ApplyUpdates.pl /path/to/.credentials.txt\n";
		exit();
	}
}
# read in config file
my %config;
open CONFIG, $file or die "can't open $file: $!";
while (<CONFIG>) {
    chomp;
    s/#.*//; # Remove comments
    s/^\s+//; # Remove opening whitespace
    s/\s+$//;  # Remove closing whitespace
    next unless length;
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
}
close CONFIG;
$userid = $config{'DBUSER'};
$userpass = $config{'DBPASS'};
$host = $config{'DBHOST'};
$datadir = $config{'DATADIR'};
$scriptpass = $config{'SCRIPTPASS'};
$scriptuser = $config{'SCRIPTUSER'};
$sitedir = $config{'SCRIPTDIR'};
# disable output buffer
$|++;


#########################
## Connect to database ##
######################### 
print "Connecting to database\n";

# get current genome build/database to update
my $dbhgb = DBI->connect("dbi:mysql:NGS-Variants-Admin:$host",$userid,$userpass);
my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
$gbsth->execute();
my @row = $gbsth->fetchrow_array();
$gbsth->finish();
$db = "NGS-Variants".$row[0];
$newbuild = $row[1];
$ucscbuild = $row[0];
$ucscbuild =~ s/^-//;
print "\t=> Using current build database (build ".$row[1].")\n";
$dbh = DBI->connect("dbi:mysql:$db:$host",$userid,$userpass) or die("Could not connect to database with credentials provided in $file");
$dbh->{mysql_auto_reconnect} = 1;
$dbh->{PrintError} = 0;
$dbh->{RaiseError} = 0;


## AIM :
## Fill in extra QC values from existing VCF files:
##  - deltaPL
##  - Stretch + length of stretch
##  - depth values (using samtools)
##  - and as a benefit : clean up data from deleted samples (if present)

## variables
my %variants = (); # keep track of previously seen variants. 
my %samples = (); # hash to hold all sids.
my %chromhash ;
%chromhash = (); 
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "X" } = 23;
$chromhash{ "Y" } = 24; 
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y";
$chromhash{ "M" } = 25;
$chromhash{ "25" } = "M";
%igvhash = ();
$igvhash{'A'} = 1;
$igvhash{'C'} = 2;
$igvhash{'G'} = 3;
$igvhash{'T'} = 4;
$igvhash{'N'} = 5;


# 1. all samples in database;
my $query = "SELECT id FROM `Samples`";
my $sth = $dbh->prepare($query);
my $nrr = $sth->execute();
$max = $nrr;
if ($nrr > 10000) {
	$max = 10000;
}
my $rowcache;
my %vxs = ();
while (my $r = shift(@$rowcache) || shift( @{$rowcache = $sth->fetchall_arrayref(undef,$max)|| []})) {
	$samples{$r->[0]} = 1;
}
$sth->finish();



my @vcfs = `cd $datadir && ls *vcf.gz`;
if (scalar(@vcfs) == 0) {
	# nothing to do. 
	exit;
}

#prepare queries
$fetchvariant = $dbh->prepare("SELECT id FROM Variants WHERE chr = ? AND start = ? AND RefAllele = ? AND AltAllele = ?");
$updatevariant = $dbh->prepare("UPDATE LOW_PRIORITY `Variants` SET Stretch =  ?, StretchUnit = ? WHERE id = ?");
$updatevxs = $dbh->prepare("UPDATE LOW_PRIORITY `Variants_x_Samples` SET StretchLengthA =  ?, StretchLengthB = ?,RefDepth = ?, AltDepth = ?,DeltaPL = ? WHERE vid = ? AND sid = ?");



foreach(@vcfs) {
	chomp;
	my $vcf = $_;
	$vcf =~ m/(\d+)\.vcf\.gz/;
	my $sid = $1;
	if (!defined($samples{$sid})) {
		print "Sample $sid is not present in database => Deleting data\n";
		#system("cd $datadir && rm $sid.*");
		next;
	}
	# Unzip
	 
	system("cd $datadir && gunzip -c $vcf > /tmp/VDB.update.vcf");
	# scan headers for 'GATK' tag (skip other files)
	open IN, "/tmp/VDB.update.vcf";
	my $gatk = 0;
	while (<IN>) {
		if (substr($_,0,6) eq '#CHROM') {
			last;
		}
		elsif ($_ =~ m/BaseQRankSum/) {
			## this is not bulletproof, as there *might* be other tools providing this QC score as well
			$gatk = 1;
		}
	}
	if ($gatk == 0) {
		# non UG/HC VCF. skip
		print "$vcf is not GATK based\n";
		close IN;
		next;
	}
	print "Processing $vcf\n";
	# process remainder of vcf
	while (<IN>) {
		chomp;
		if ($_ eq '') {
			next;
		}
		my @p = split(/\t/,$_);
		# details from variant file
		my $chr = $p[0];
		$chr =~ s/chr//;
		$chr = $chromhash{$chr}; # convert XYM to numerical values.
		my $start = $p[1];
		my $refall = $p[3];
		my $stop = $start;
		if (length($refall) > 1) {
			$stop = $start + length($refall) - 1;
		}
		my $altalls = $p[4];
		my @format = split(/:/,$p[8]);
		my @sample = split(/:/,$p[9]);
		my %shash = ();
		my $idx = -1;
		# create format => value hash.
		foreach (@format) {
			$idx++;
			$shash{$_} = $sample[$idx];
		}
		my @salleles = split(/\//,$shash{'GT'});
		my @info = split(/;/,$p[7]) ;
		## parse INFO fields
		my %ihash;
		foreach (@info) {
			my ($ifield, $ival) = split(/=/,$_);
			$ihash{$ifield} = $ival;
		}
		# in stretch? 
		my $str = $strA = $strB = 0;
		my $stru = '';
		if (defined($ihash{'RU'})) {
			$str = 1;
			$stru = $ihash{'RU'};
			($strA,$strB) = split(/,/,$ihash{'RPA'});
		}
		my $dpl = 0;
		if (defined($shash{'PL'})) {
			my @pls = split(/,/,$shash{'PL'});
			@pls = sort {$a <=> $b} @pls;
			$dpl = $pls[1];	
		}
		if (scalar(@salleles) == 2) {
			#my $ploidy = 2;
			$firstall = $salleles[0];
			$secondall = $salleles[1];
			$altcount = $firstall + $secondall;
			my @depths = split(/,/,$shash{'AD'});
			$refdepth = $depths[0];
			## multiallelic ?
			my @aa = split(/,/,$altalls);
			## multiallelic if altcount > 2 && $firstall ne $secondall : two different alternative alleles present.
			if (scalar(@aa) > 1  && $altcount > 2 && $firstall != $secondall) {
				$altcount = 1; # set to heterozygous (two different alternative alleles)
			}
			else {
				if ($altcount > 2) {
					$altcount = 2; # set to homozygous (twice same alternative allele (but not the first listed).
				}
			}
			my $aaidx = 0;
			foreach (@aa) {
				$aaidx++;
				my $altdepth;
				## reference call should be allowed (hack on custom VCF files with identical ref/alt cols).
				if ($_ eq $refall && $altcount == 0) {
					$altdepth = $depths[1]; # take first alternative allele, to have info on background.
				}
				## reference call should be allowed (reference call on polymorphic position (eg RNA/tumor).
				if ($altcount == 0) {
					$altdepth = $depths[1]; # take first alternative allele, to have info on background.
				}

				## non-present alleles should be skipped
				elsif ($firstall != $aaidx && $secondall != $aaidx) {
					# this allele (0 = ref, 1+ are alt alleles) is not present in this sample.
					next;
				}
				## present alleles: set depth
				else {
					$altdepth = $depths[$aaidx];
				}
				my $altall = $_;
				# get variant id.
				# 1. from hash
				my $vid = 0;
				if (defined($variants{$start."-".$refall."-".$altall})) {
					$vid = $variants{$start."-".$refall."-".$altall};
				}
				# 2. recheck db for race conditions with other inserts
				else {
					## lock on status file to prevent inserts/race conditions
					my $found = $fetchvariant->execute($chr,$start,$refall,$altall) or die('Could not fetch single variant');
					if ($found >= 1) {
						($vid) = $fetchvariant->fetchrow_array();
						$variants{$start."-".$refall."-".$altall} = $vid;
						## update variant with str info.
						if ($str == 1) {
							$updatevariant->execute($str,$stru,$vid);
						}
					}	
				}
				## fetch missing depths with igvtools.		    
				if ($refdepth eq '' || $altdepth eq '') {
					if (length($refall) == 1 || length($altall) == 1) {
						if (-e "$datadir/$sid.bam") {
							system("echo $scriptpass | sudo -u $scriptuser -S bash -c \"$sitedir/IGVTools_bin/igvtools count -w 1 --bases --query chr$chr:$start-$start $datadir/$sid.bam /tmp/AF.$sid.wig hg19 && chmod 777 /tmp/AF.$sid.wig\"");

							my $lline = `tail -n 1 "/tmp/AF.$sid.wig"`;
							chomp($lline);
							unlink("/tmp/AF.$sid.wig");
							my @igvline = split(/\s+/,$lline);
							if ($refdepth eq '') {
								$refdepth = sprintf("%.0f",$igvline[$igvhash{$refall}]);
							}
							if ($altdepth eq '') {
								$altdepth = sprintf("%.0f",$igvline[$igvhash{$altall}]);
								#print POUT "$lineidx : ivgbased altdepth to $altdepth\n";
							}
						}
			
					}
				}
				## update variants_x_samples
				$updatevxs->execute($strA,$strB,$refdepth,$altdepth,$dpl,$vid,$sid);	
			} ## goto next alternative allele.
		}
		## THIS IS FOR HIGH PLOIDY SAMPLES
		else {
			my $ploidy = scalar(@salleles);
			my @depths = split(/,/,$shash{'AD'});
			$refdepth = $depths[0];
			## get GT-counts per allele.
			my %sas;
			foreach(@salleles) {
				if (!defined($sas{$_})) {
					$sas{$_} = 0;
				}
				$sas{$_}++;
			}

			## multiallelic ?
			my @aa = split(/,/,$altalls);
			## multiallelic if altcount > 2 && two different numbers in @sample_alleles, not equal to 0 (ref)
			my %altsas = %sas;
			if (defined($altsas{'0'})) {
				delete($altsas{'0'});
			}
			if (scalar(@aa) > 1  && keys(%altsas) > 1) {
				$altcount = 1; # set to heterozygous (two different alternative alleles), possibly + ref.
			}
			else {
				# no zero's, not multiallelic => hom.alt
				if (!defined($sas{'0'})) {
					$altcount = 2; # set to homozygous (no reference allele present).
				}
				# two alleles, not multiallelic => het
				if (keys(%sas) > 1) {
					$altcount = 1;
				}
				# else : hom.ref
				else {
					$altcount = 0;
				}
			}
			# process alleles
			my $aaidx = 0;
			foreach (@aa) {
				$aaidx++;
				if (!defined($sas{$aaidx})) {
					# this allele (0 = ref, 1+ are alt alleles) is not present in this sample.
					next;
				}
				my $altall = $_;
				my $altdepth = $depths[$aaidx];
				# get variant id.
				# 1. from hash
				my $vid = 0;
				if (defined($variants{$start."-".$refall."-".$altall})) {
					$vid = $variants{$start."-".$refall."-".$altall};
				}
				# 2. recheck db for race conditions with other inserts
				else {
					## lock on status file to prevent inserts/race conditions
					my $found = $fetchvariant->execute($chr,$start,$refall,$altall) or die('Could not fetch single variant');
					if ($found >= 1) {
						($vid) = $fetchvariant->fetchrow_array();
						$variants{$start."-".$refall."-".$altall} = $vid;
						## update variant with str info.
						if ($str == 1) {
							$updatevariant->execute($str,$stru,$vid);
						}
					}
				}
				## fetch missing depths with igvtools.		    
				if ($refdepth eq '' || $altdepth eq '') {
					if (length($refall) == 1 || length($altall) == 1) {
						if (-e "$datadir/$sid.bam") {
							system("echo $scriptpass | sudo -u $scriptuser -S bash -c \"$sitedir/IGVTools_bin/igvtools count -w 1 --bases --query chr$chr:$start-$start $datadir/$sid.bam /tmp/AF.$sid.wig hg19 && chmod 777 /tmp/AF.$sid.wig\"");
							my $lline = `tail -n 1 "/tmp/AF.$sid.wig"`;
							chomp($lline);
							unlink("/tmp/AF.$sid.wig");
							my @igvline = split(/\s+/,$lline);
							if ($refdepth eq '') {
								$refdepth = sprintf("%.0f",$igvline[$igvhash{$refall}]);
							}	
							if ($altdepth eq '') {
								$altdepth = sprintf("%.0f",$igvline[$igvhash{$altall}]);
							}
						}	
					}
				}
				## update variants_x_samples
				$updatevxs->execute($strA,$strB,$refdepth,$altdepth,$dpl,$vid,$sid);	

				
			} ## goto next alternative allele.

		}



	}
	close IN;
	# remove tmp vcf
	system("rm /tmp/VDB.update.vcf");
}

# finish queries
$fetchvariant->finish();
$updatevariant->finish();
$updatevxs->finish();
$dbh->disconnect();









