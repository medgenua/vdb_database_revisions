#!/usr/bin/perl

use DBI;
use Getopt::Std;

my $file = $ARGV[0];
if ($file eq '') {

	# check default cred file
	if (-e '../.Credentials/.credentials') {
		$file = '../.Credentials/.credentials';
	}
	else{
		print "Path to credentials file not provided.\n";
		print " USAGE: perl ApplyUpdates.pl /path/to/.credentials.txt\n";
		exit();
	}
}
# read in config file
my %config;
open CONFIG, $file or die "can't open $file: $!";
while (<CONFIG>) {
    chomp;
    s/#.*//; # Remove comments
    s/^\s+//; # Remove opening whitespace
    s/\s+$//;  # Remove closing whitespace
    next unless length;
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
}
close CONFIG;
$userid = $config{'DBUSER'};
$userpass = $config{'DBPASS'};
$host = $config{'DBHOST'};
# disable output buffer
$|++;


# argument to skip some steps if performing initial installation.
$install = 0;
if (defined($ARGV[1]) && $ARGV[1] eq 'install') {
	$install = 1;
}



#########################
## Connect to database ##
######################### 
print "Connecting to database\n";

# get current genome build/database to update
my $dbhgb = DBI->connect("dbi:mysql:NGS-Variants-Admin:$host",$userid,$userpass);
my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
$gbsth->execute();
my @row = $gbsth->fetchrow_array();
$gbsth->finish();
$db = "NGS-Variants".$row[0];
$newbuild = $row[1];
$ucscbuild = $row[0];
$ucscbuild =~ s/^-//;
print "\t=> Using current build database (build ".$row[1].")\n";
$dbh = DBI->connect("dbi:mysql:$db:$host",$userid,$userpass) or die("Could not connect to database with credentials provided in $file");
$dbh->{mysql_auto_reconnect} = 1;
$dbh->{PrintError} = 0;
$dbh->{RaiseError} = 0;


###### AIM : 
###### Add individual aid indices on tables without them (is mandatory for MEMORY tables if autoincrement). 

## get all tables.
my $sth = $dbh->prepare("SHOW TABLES LIKE 'Variants_x_%'");
my $nrr = $sth->execute();
my $rowcache;
print "Nr of tables to evaluate: $nrr\n";
while (my $r = shift(@$rowcache) || shift( @{$rowcache = $sth->fetchall_arrayref()|| []})) {
	my $table = $r->[0];
	#$dbh->do("DROP INDEX `aid` ON $table");
	my $query = "SELECT COUNT(1) IndexIsThere FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema='$db' AND table_name='$table' AND index_name='aid'";
	my $isth = $dbh->prepare($query);
	$isth->execute();
	my $row = $isth->fetchrow_arrayref();
	my $idx_name = "aid";
	if ($row->[0] == 1) {
		print "Table '$table' : index on aid present\n";
		next;
	}
	elsif($row->[0] > 1) {
		print "Table has multi-column index named 'aid'. Creating alternate name\n";
		$idx_name = "aid_single";
	}
	$isth->finish();
	# no/multiple-col index found. Only create if auto-indexed column.
	$query = "SELECT EXTRA FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='$table' AND COLUMN_NAME='aid'";
	$isth = $dbh->prepare($query);
	my $nr = $isth->execute();
	# no aid col? (should not happen)
	if ($nr == 0) {
		next;
	}
	$row = $isth->fetchrow_arrayref();
	$isth->finish();
	if ($row->[0] =~ m/auto_increment/) {
		print "Table '$table' : index on auto_incrementing aid absent. Creating now.\n";
		$dbh->do("CREATE INDEX $idx_name ON $table (aid)") or die("Could not create error : $DBI::errstr\n");
	}
}
$sth->finish();
		

