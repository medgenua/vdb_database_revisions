#!/usr/bin/perl

## AIM:
# scan all currently available datafiles per sample, and add to db.

use DBI;
use Getopt::Std;
my $file = $ARGV[0];
if ($file eq '') {
	# check default cred file
	if (-e '../.Credentials/.credentials') {
		$file = '../.Credentials/.credentials';
	}
	else {
		print "Path to credentials file not provided.\n";
		print " USAGE: perl ApplyUpdates.pl /path/to/.credentials.txt\n";
		exit();
	}
}
# read in config file
my %config;
open CONFIG, $file or die "can't open $file: $!";
while (<CONFIG>) {
    chomp;
    s/#.*//; # Remove comments
    s/^\s+//; # Remove opening whitespace
    s/\s+$//;  # Remove closing whitespace
    next unless length;
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
}
close CONFIG;
$userid = $config{'DBUSER'};
$userpass = $config{'DBPASS'};
$host = $config{'DBHOST'};
$datadir = $config{'DATADIR'};
# disable output buffer
$|++;

# argument to skip some steps if performing initial installation.
$install = 0;
if (defined($ARGV[1]) && $ARGV[1] eq 'install') {
	$install = 1;
	# update on stored datafiles. There are no files yet.
	exit;
}


#########################
## Connect to database ##
######################### 
print "Connecting to database\n";

# get current genome build/database to update
my $dbhgb = DBI->connect("dbi:mysql:NGS-Variants-Admin:$host",$userid,$userpass);
my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
$gbsth->execute();
my @row = $gbsth->fetchrow_array();
$gbsth->finish();
$db = "NGS-Variants".$row[0];
$newbuild = $row[1];
$ucscbuild = $row[0];
$ucscbuild =~ s/^-//;
print "\t=> Using current build database (build ".$row[1].")\n";
$dbh = DBI->connect("dbi:mysql:$db:$host",$userid,$userpass) or die("Could not connect to database with credentials provided in $file");
$dbh->{mysql_auto_reconnect} = 1;
$dbh->{PrintError} = 0;
$dbh->{RaiseError} = 0;


my %samples = ();
my $sth = $dbh->prepare("SELECT id FROM `Samples`");
$sth->execute();
while (my @row = $sth->fetchrow_array()) {
	$samples{$row[0]} = 0;
}
$sth->finish();
#my $sth = $dbh->prepare("UPDATE `Samples` SET `Stored_Filesize` = `Stored_FileSize + ? WHERE id = ?");

my @files = `cd $datadir ; ls *.vcf.gz* *.bam*`;
chomp(@files);
foreach my $file (@files) {
	my $filesize = -s "$datadir/$file";
	$file =~ m/(\d+)\.*/;
	my $sample = $1;
	if (defined($samples{$sample})) {
		$samples{$sample} += $filesize;
	}
	else {
		print "Sample ID '$sample' no longer in database. This file can be deleted.\n";
	}


}
my $sth = $dbh->prepare("UPDATE `Samples` SET `Stored_Filesize` = ? WHERE id = ?");
foreach my $sample (keys(%samples)) {
	$sth->execute($samples{$sample},$sample);
}
$sth->finish();
