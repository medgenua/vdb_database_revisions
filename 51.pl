#!/usr/bin/perl

use DBI;
use Getopt::Std;

my $file = $ARGV[0];
if ($file eq '') {

	# check default cred file
	if (-e '../.Credentials/.credentials') {
		$file = '../.Credentials/.credentials';
	}
	else{
		print "Path to credentials file not provided.\n";
		print " USAGE: perl ApplyUpdates.pl /path/to/.credentials.txt\n";
		exit();
	}
}
# read in config file
my %config;
open CONFIG, $file or die "can't open $file: $!";
while (<CONFIG>) {
    chomp;
    s/#.*//; # Remove comments
    s/^\s+//; # Remove opening whitespace
    s/\s+$//;  # Remove closing whitespace
    next unless length;
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
}
close CONFIG;
$userid = $config{'DBUSER'};
$userpass = $config{'DBPASS'};
$host = $config{'DBHOST'};
$datadir = $config{'DATADIR'};
$scriptpass = $config{'SCRIPTPASS'};
$scriptuser = $config{'SCRIPTUSER'};
$scriptdir = $config{'SCRIPTDIR'};
# disable output buffer
$|++;

# argument to skip some steps if performing initial installation.
$install = 0;
if (defined($ARGV[1]) && $ARGV[1] eq 'install') {
	$install = 1;
	# table updates. are empty. skip
	exit;
}


#########################
## Connect to database ##
######################### 
print "Connecting to database\n";

# get current genome build/database to update
my $dbhgb = DBI->connect("dbi:mysql:NGS-Variants-Admin:$host",$userid,$userpass);
my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
$gbsth->execute();
my @row = $gbsth->fetchrow_array();
$gbsth->finish();
$db = "NGS-Variants".$row[0];
$newbuild = $row[1];
$ucscbuild = $row[0];
$ucscbuild =~ s/^-//;
print "\t=> Using current build database (build ".$row[1].")\n";
$dbh = DBI->connect("dbi:mysql:$db:$host",$userid,$userpass) or die("Could not connect to database with credentials provided in $file");
$dbh->{mysql_auto_reconnect} = 1;
$dbh->{PrintError} = 0;
$dbh->{RaiseError} = 0;


## AIM: 
## UPDATE ANNOVAR changed names in gene effect annotations in:
#	- Value_codes (refgene, knowngene, ensgene) : get old and new values (w/wo SNV
#	- Variants_x_XXgene => replace old by new value
# 	- Go through all stored filters, update 
#	- Delete from value codes.

my @tables = qw/refgene ensgene knowngene/;
my %caps = ('refgene' => 'RefSeq', 'ensgene' => 'Ensembl', 'knowngene' => 'UCSC');
foreach(@tables) {
	my $tn = $_;
	my $t = "Variants_x_ANNOVAR_".$tn;
	my $vc = $t."_VariantType";
	#1.  prep to get IDs for the items to update.
	my $sth = $dbh->prepare("SELECT id FROM `Value_Codes` WHERE Table_x_Column = '$vc' AND  Item_Value = ?");
	# old
	$sth->execute("stopgain SNV");
	my ($og) = $sth->fetchrow_array();
	$sth->execute("stoploss SNV");
	my ($ol) = $sth->fetchrow_array();
	# new
	$sth->execute("stopgain");
	my ($ng) = $sth->fetchrow_array();
	$sth->execute("stoploss");
	my ($nl) = $sth->fetchrow_array();
	$sth->finish();
	#2. UPDATE TABLES
	$dbh->do("UPDATE TABLE `$t` SET VariantType = '$ng' WHERE VariantType = '$og'");
	$dbh->do("UPDATE TABLE `$t` SET VariantType = '$nl' WHERE VariantType = '$ol'");
	##3. UPDATE FILTERS
	# prepare query to restore.
	$upd = $dbh->prepare("UPDATE `Users_x_FilterSettings` SET FilterSettings = ?, FilterTree = ? WHERE fid = ?");
	# fetch filters
	$sth = $dbh->prepare("SELECT fid, FilterSettings, FilterTree FROM `Users_x_FilterSettings`");
	$sth->execute();
	my $rowcache = $sth->fetchall_arrayref();
	while (my $aref = shift(@$rowcache)) {
		# split settings into array
		my @settings = split(/@@@/,$aref->[1]);
		## get idx of items to do, put all in hash while doing so.
		my %h = ();
		my @todo = ();
		my @keys;
		foreach(@settings) {
			my ($k,$v) = split(/\|\|\|/,$_);
			$h{$k} = $v;
			push(@keys,$k);
			my $re = $caps{$tn}."_VariantType";
			if ($v =~ m/$re/) {
				$k =~ m/param(\d+)/;
				push(@todo,$1);
			}
		}
		## update arguments of k/v pairs corresponding to param == RS_VT
		foreach(@todo) {
			my $idx = $_;
			# split arguments.
			my $nval = '';
			my @vals = split(/__/,$h{'argument'.$idx});
			my %done = ();
			foreach(@vals) {
				$_ =~ s/^$og$/$ng/;
				$_ =~ s/^$ol$/$nl/;
				if (defined($done{$_})) {
					# skip doubls after replacement
					next;
				}
				$done{$_} = 1;
				$nval .= $_."__";
			}
			$h{"argument".$idx} = $nval;
			
		}
		## recombine settings.
		my $nsets = '';
		foreach(@keys) {
			$nsets .= $_.'|||'.$h{$_}."@@@";
		}
		$nsets = substr($nsets,0,-3);
		## replace in filtertree.
		$aref->[2] =~ s/stopgain SNV/stopgain/g;
		$aref->[2] =~ s/stoploss SNV/stoploss/;
		$upd->execute($nsets,$aref->[2],$aref->[0]);
	}
	$upd->finish();
	## 4. DELETE FROM VALUE CODES
	$dbh->do("DELETE FROM `Value_Codes` WHERE id IN ('$og','$ol')");
}	
