ALTER TABLE `NGS-Variants-hg19`.`Classifiers_x_Variants` DROP PRIMARY KEY;
ALTER TABLE  `NGS-Variants-hg19`.`Classifiers_x_Variants` ADD PRIMARY KEY (  `cid` ,  `vid` ,  `genotype` ) ;
ALTER TABLE  `NGS-Variants-hg19`.`Classifiers_x_Variants` ADD  `validate_reject` TINYINT( 1 ) NOT NULL DEFAULT  '0' AFTER  `added_on` ;
ALTER TABLE  `NGS-Variants-hg19`.`Classifiers_x_Variants` ADD INDEX (  `validate_reject` ) ;

