#!/usr/bin/perl

use DBI;
use Getopt::Std;

my $file = $ARGV[0];
if ($file eq '') {

        # check default cred file
        if (-e '../.Credentials/.credentials') {
                $file = '../.Credentials/.credentials';
        }
        else{
                print "Path to credentials file not provided.\n";
                print " USAGE: perl ApplyUpdates.pl /path/to/.credentials.txt\n";
                exit();
        }
}
# read in config file
my %config;
open CONFIG, $file or die "can't open $file: $!";
while (<CONFIG>) {
    chomp;
    s/#.*//; # Remove comments
    s/^\s+//; # Remove opening whitespace
    s/\s+$//;  # Remove closing whitespace
    next unless length;
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
}
close CONFIG;
$userid = $config{'DBUSER'};
$userpass = $config{'DBPASS'};
$host = $config{'DBHOST'};
$scriptdir = $config{'SCRIPTDIR'};
$ftpdir = $config{'FTP_DATA'};
# disable output buffer
$|++;


# GOAL : download <build>.2bit sequence for splice prediction.



my $target_dir = "$scriptdir/Annotations/ANNOVAR/humandb/";

if (!-d $target_dir) {
	die("Target folder is not available: '$target_dir'");
}

#########################
## Connect to database ##
######################### 
print "Connecting to database\n";

# get current genome build/database to update
my $dbhgb = DBI->connect("dbi:mysql:NGS-Variants-Admin:$host",$userid,$userpass);
my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
$gbsth->execute();
my @row = $gbsth->fetchrow_array();
$gbsth->finish();
$db = "NGS-Variants".$row[0];
$newbuild = $row[1];
$ucscbuild = $row[0];
$ucscbuild =~ s/^-//;
print "\t=> Using current build database (build ".$row[1].")\n";
$dbh = DBI->connect("dbi:mysql:$db:$host",$userid,$userpass) or die("Could not connect to database with credentials provided in $file");
$dbh->{mysql_auto_reconnect} = 1;
$dbh->{PrintError} = 0;
$dbh->{RaiseError} = 0;


my $target_file = "$target_dir/$ucscbuild.2bit";
my $url = "http://hgdownload.soe.ucsc.edu/goldenPath/$ucscbuild/bigZips/$ucscbuild.2bit";
print "Download '$ucscbuild.2bit' from the UCSC servers. This is approximately 800Mb.\n";

system("wget -q -O '$target_file' '$url'");


