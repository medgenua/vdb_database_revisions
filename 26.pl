#!/usr/bin/perl

use DBI;
use Getopt::Std;
my $file = $ARGV[0];
if ($file eq '') {
	# check default cred file
	if (-e '../.Credentials/.credentials') {
		$file = '../.Credentials/.credentials';
	}
	else {
		print "Path to credentials file not provided.\n";
		print " USAGE: perl ApplyUpdates.pl /path/to/.credentials.txt\n";
		exit();
	}
}
# read in config file
my %config;
open CONFIG, $file or die "can't open $file: $!";
while (<CONFIG>) {
    chomp;
    s/#.*//; # Remove comments
    s/^\s+//; # Remove opening whitespace
    s/\s+$//;  # Remove closing whitespace
    next unless length;
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
}
close CONFIG;
$userid = $config{'DBUSER'};
$userpass = $config{'DBPASS'};
$host = $config{'DBHOST'};
# disable output buffer
$|++;

# argument to skip some steps if performing initial installation.
$install = 0;
if (defined($ARGV[1]) && $ARGV[1] eq 'install') {
	$install = 1;
}



#########################
## Connect to database ##
######################### 
print "Connecting to database\n";

# get current genome build/database to update
my $dbhgb = DBI->connect("dbi:mysql:NGS-Variants-Admin:$host",$userid,$userpass);
my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
$gbsth->execute();
my @row = $gbsth->fetchrow_array();
$gbsth->finish();
$db = "NGS-Variants".$row[0];
$newbuild = $row[1];
$ucscbuild = $row[0];
$ucscbuild =~ s/^-//;
print "\t=> Using current build database (build ".$row[1].")\n";
$dbh = DBI->connect("dbi:mysql:$db:$host",$userid,$userpass) or die("Could not connect to database with credentials provided in $file");
$dbh->{mysql_auto_reconnect} = 1;
$dbh->{PrintError} = 0;
$dbh->{RaiseError} = 0;


###### AIM : 
###### Optimize tables after potentially big changes in rev 25.

# define target tables
@tables = qw(Variants_x_ANNOVAR_1000g2012apr_afr Variants_x_ANNOVAR_1000g2012apr_all
Variants_x_ANNOVAR_1000g2012apr_amr
Variants_x_ANNOVAR_1000g2012apr_asn
Variants_x_ANNOVAR_1000g2012apr_eur
Variants_x_ANNOVAR_ensgene
Variants_x_ANNOVAR_esp5400_aa
Variants_x_ANNOVAR_esp5400_all
Variants_x_ANNOVAR_esp5400_ea
Variants_x_ANNOVAR_esp6500si_aa
Variants_x_ANNOVAR_esp6500si_all
Variants_x_ANNOVAR_esp6500si_ea
Variants_x_ANNOVAR_knowngene
Variants_x_ANNOVAR_ljb_gerp
Variants_x_ANNOVAR_ljb_lrt
Variants_x_ANNOVAR_ljb_mt
Variants_x_ANNOVAR_ljb_phylop
Variants_x_ANNOVAR_ljb_pp2
Variants_x_ANNOVAR_ljb_sift
Variants_x_ANNOVAR_refgene
Variants_x_ANNOVAR_segdup
Variants_x_ANNOVAR_snp130
Variants_x_ANNOVAR_snp135
Variants_x_ANNOVAR_snp137
Variants_x_GO
Variants_x_MutationTaster
Variants_x_SIFT
Variants_x_snpEff_GRCh37.66
Variants );

#loop and prune process tables

foreach(@tables) {
	#my %lvxs = %vxs;
	my $table = $_;
	print "Optimizing table '$table'\n";
	# local disables bin_logs, as they are not used.
	$dbh->do("OPTIMIZE LOCAL TABLE `$table`");

}
		

