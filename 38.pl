#!/usr/bin/perl

use DBI;
use Getopt::Std;

my $file = $ARGV[0];
if ($file eq '') {

	# check default cred file
	if (-e '../.Credentials/.credentials') {
		$file = '../.Credentials/.credentials';
	}
	else{
		print "Path to credentials file not provided.\n";
		print " USAGE: perl ApplyUpdates.pl /path/to/.credentials.txt\n";
		exit();
	}
}
# read in config file
my %config;
open CONFIG, $file or die "can't open $file: $!";
while (<CONFIG>) {
    chomp;
    s/#.*//; # Remove comments
    s/^\s+//; # Remove opening whitespace
    s/\s+$//;  # Remove closing whitespace
    next unless length;
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
}
close CONFIG;
$userid = $config{'DBUSER'};
$userpass = $config{'DBPASS'};
$host = $config{'DBHOST'};
$datadir = $config{'DATADIR'};
$scriptpass = $config{'SCRIPTPASS'};
$scriptuser = $config{'SCRIPTUSER'};
$sitedir = $config{'SCRIPTDIR'};
$scriptdir = $config{'SCRIPTDIR'};
# disable output buffer
$|++;

# argument to skip some steps if performing initial installation.
$install = 0;
if (defined($ARGV[1]) && $ARGV[1] eq 'install') {
	$install = 1;
	# update on tables (skip, as they are empty)
	# annotation job. skipped
	exit;
}


#########################
## Connect to database ##
######################### 
print "Connecting to database\n";

# get current genome build/database to update
my $dbhgb = DBI->connect("dbi:mysql:NGS-Variants-Admin:$host",$userid,$userpass);
my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
$gbsth->execute();
my @row = $gbsth->fetchrow_array();
$gbsth->finish();
$db = "NGS-Variants".$row[0];
$newbuild = $row[1];
$ucscbuild = $row[0];
$ucscbuild =~ s/^-//;
print "\t=> Using current build database (build ".$row[1].")\n";
$dbh = DBI->connect("dbi:mysql:$db:$host",$userid,$userpass) or die("Could not connect to database with credentials provided in $file");
$dbh->{mysql_auto_reconnect} = 1;
$dbh->{PrintError} = 0;
$dbh->{RaiseError} = 0;


## AIM :
# strip static build values from : value_codes, users_x_filters, users_x_annotations
# correct saved filters if they contain non-coded value_settings.
# restructure GeneXRef storage in ClinVar


## variables
my %chromhash ;
%chromhash = (); 
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "X" } = 23;
$chromhash{ "Y" } = 24; 
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y";
$chromhash{ "M" } = 25;
$chromhash{ "25" } = "M";


# 1. load & update value_codes
my $query = "SELECT id, Table_x_Column, Item_Value FROM `Value_Codes`";
my $sth = $dbh->prepare($query);
$sth->execute();
my $rowcache = $sth->fetchall_arrayref();
my %values = ();
$sth->finish();
while (my $r = shift(@$rowcache) ) {
	if ($r->[1] =~ m/_GRCh37\.66/) {
		$r->[1] =~ s/_GRCh37\.66//g;
		$dbh->do("UPDATE `Value_Codes` SET Table_x_Column = '".$r->[1]."' WHERE id = '".$r->[0]."'");
	}
	$values{$r->[1]}{$r->[2]} = $r->[0];
	#print "$r->[1] -> $r->[2] = $r->[0]\n";
}

# 2. correct saved annotations
$sth = $dbh->prepare("SELECT aid, AnnotationSettings FROM `Users_x_Annotations`");
$sth->execute();
$rowcache = $sth->fetchall_arrayref();
$sth->finish();
my $usth = $dbh->prepare("UPDATE `Users_x_Annotations` SET AnnotationSettings = ? WHERE aid = ?");
while (my $r = shift(@$rowcache) ) {
	if ($r->[1] =~ m/_37\.36/) {
		$r->[1] =~ s/_37\.66//g;
		$usth->execute($r->[1],$r->[0]);
	}
}
$usth->finish();

# 2. correct saved filters
my %replace = ('RefSeq_' => "Variants_x_ANNOVAR_refgene_", 'UCSC_' => "Variants_x_ANNOVAR_knowngene_", 'Ensembl_' => 'Variants_x_ANNOVAR_ensgene_', 'Effect_Impact' => 'Variants_x_snpEff_EffectImpact','Functional_Class' => 'Variants_x_snpEff_FunctionalClass', 'Gene_Coding' => 'Variants_x_snpEff_GeneCoding','Transcript_BioType','Variants_x_snpEff_TranscriptBiotype','Effect' => 'Variants_x_snpEff_Effect');
$sth = $dbh->prepare("SELECT fid, FilterSettings FROM `Users_x_FilterSettings`");
$sth->execute();
$rowcache = $sth->fetchall_arrayref();
$sth->finish();
my $usth = $dbh->prepare("UPDATE `Users_x_FilterSettings` SET FilterSettings = ? WHERE fid = ?");
while (my $r = shift(@$rowcache) ) {
	$update = 0;
	# replace snpEff naming
	if ($r->[1] =~ m/_Annotations_from_GRCh37_66/) {
		$r->[1] =~ s/_Annotations_from_GRCh37_66//g;
		$update = 0;
	}
	# replace non-coded values
	my @v = split(/@@@/,$r->[1]);
	my %fv = ();
	foreach(@v) {
		my ($a,$b) = split(/\|\|\|/,$_);
		$a =~ m/(\D+)(\d+)/;
		$fv{$2}{$1} = $b;
	}
	foreach(keys(%fv)) {
		my $nr = $_;
		if (!defined($fv{$nr}{'argument'})) {
			next;
		}
		my @vs = split(/__/,$fv{$nr}{"argument"});
		$fv{$nr}{'argument'} = '';
		foreach(@vs) {
			my $val = $_;
			my $par = $fv{$nr}{'param'};
			foreach(keys(%replace)) {
				if ($par =~ m/$_/) {
					$par =~ s/$_/$replace{$_}/;
					last;
				}
			}		
	
			if (defined($values{$par}{$val})) {
				$val = $values{$par}{$val};
				#print "Defined: $val in $par\n";
				$update = 1;
			}
			#else {
			#	print "NotDefined : $val in $par\n";
			#}
			$fv{$nr}{'argument'} .= $val . "__";
		}
	}
	my @k = sort {$a <=> $b} keys(%fv);
	my $string = '';
	foreach(@k) {
		$string .= "category$_".'|||'.$fv{$_}{'category'}.'@@@'."negate$_".'|||'.$fv{$_}{'negate'}.'@@@'."param$_".'|||'.$fv{$_}{'param'}.'@@@';
		if (defined($fv{$_}{'argument'})) {
			$string .= "argument$_".'|||'.$fv{$_}{'argument'}.'@@@';
		}
		if (defined($fv{$_}{'value'})) {
			$string .= "value$_".'|||'.$fv{$_}{'value'}.'@@@';
		}
	}
	if ($string ne '') {
		#print "$r->[1]\n========\n";
		$string = substr($string,0,-3);
		#print "$string\n\n############################\n";
		$usth->execute($string,$r->[0]);
	}
}
$usth->finish();



$dbh->disconnect();

## repopulate clinVar in new format (GeneXRef as full links) + RCV as ID instead of ClinVarSET ID (which is not informative)
system("cd $scriptdir/Annotations/ClinVar/ && perl UpdateClinVar.pl");

## reannotate variants using this new format (Variants_x_ClinVar was truncated)
system("cd $scriptdir/Annotations/ClinVar/ && perl LoadVariants.pl");




