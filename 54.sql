CREATE TABLE  `NGS-Variants-hg19`.`Report_Sections` (`rsid` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,`Name` VARCHAR( 255 ) NOT NULL ,`Description` TEXT NOT NULL ,`Owner` INT NOT NULL ,`Public` TINYINT( 1 ) NOT NULL DEFAULT  '0',`FilterSet` INT NOT NULL ,`Annotations` TEXT NOT NULL ,INDEX (  `Name` )) ENGINE = MYISAM ;
ALTER TABLE `NGS-Variants-hg19`.`Report_Sections` ADD INDEX (  `Public` );
ALTER TABLE  `NGS-Variants-hg19`.`Log` ADD INDEX (  `vid` );
