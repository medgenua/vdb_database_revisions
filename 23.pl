#!/usr/bin/perl

use DBI;
use Getopt::Std;

my $file = $ARGV[0];
if ($file eq '') {

	# check default cred file
	if (-e '../.Credentials/.credentials') {
		$file = '../.Credentials/.credentials';
	}
	else {
		print "Path to credentials file not provided.\n";
		print " USAGE: perl ApplyUpdates.pl /path/to/.credentials.txt\n";
		exit();
	}
}
# read in config file
my %config;
open CONFIG, $file or die "can't open $file: $!";
while (<CONFIG>) {
    chomp;
    s/#.*//; # Remove comments
    s/^\s+//; # Remove opening whitespace
    s/\s+$//;  # Remove closing whitespace
    next unless length;
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
}
close CONFIG;
$userid = $config{'DBUSER'};
$userpass = $config{'DBPASS'};
$host = $config{'DBHOST'};
# disable output buffer
$|++;

# argument to skip some steps if performing initial installation.
$install = 0;
if (defined($ARGV[1]) && $ARGV[1] eq 'install') {
	$install = 1;
	## only updates on tables => now all are empty, so skip this script
	exit;
}



#########################
## Connect to database ##
######################### 
print "Connecting to database\n";

# get current genome build/database to update
my $dbhgb = DBI->connect("dbi:mysql:NGS-Variants-Admin:$host",$userid,$userpass);
my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
$gbsth->execute();
my @row = $gbsth->fetchrow_array();
$gbsth->finish();
$db = "NGS-Variants".$row[0];
$newbuild = $row[1];
$ucscbuild = $row[0];
$ucscbuild =~ s/^-//;
print "\t=> Using current build database (build ".$row[1].")\n";
$dbh = DBI->connect("dbi:mysql:$db:$host",$userid,$userpass) or die("Could not connect to database with credentials provided in $file");
$dbh->{mysql_auto_reconnect} = 1;
$dbh->{PrintError} = 0;
$dbh->{RaiseError} = 0;


###### AIM : 
###### Replace recurrent annotations by numeric codes.

#############################
## 1: RefSeq Gene Location ##
#############################
my %rghash = ();
# fetch distinct values
my $sth = $dbh->prepare("SELECT DISTINCT(GeneLocation) From `Variants_x_ANNOVAR_refgene`");
$sth->execute();
my $aref = $sth->fetchall_arrayref();
$sth->finish();
## add to database, and get idx.
$sth = $dbh->prepare("INSERT INTO `Value_Codes` (Table_x_Column,Item_Value) VALUES ('Variants_x_ANNOVAR_refgene_GeneLocation',?)");
foreach(@$aref) {
	$sth->execute($_->[0]);
	$last_insert = $dbh->last_insert_id(undef,undef, undef,undef);
	#print "$last_insert => $_->[0]\n";
	$rghash{$_->[0]} = $last_insert;
}
## replace in database.
$sth->finish();
$sth = $dbh->prepare("UPDATE `Variants_x_ANNOVAR_refgene` SET GeneLocation = ? WHERE GeneLocation = ?");
foreach(keys(%rghash)) {
	my $replace = $_;
	my $intval = $rghash{$_};
	$sth->execute($intval,$replace);
}
$sth->finish();
#############################
## 1: RefSeq Gene VariantType ##
#############################
%rghash = ();
# fetch distinct values
my $sth = $dbh->prepare("SELECT DISTINCT(VariantType) From `Variants_x_ANNOVAR_refgene`");
$sth->execute();
my $aref = $sth->fetchall_arrayref();
$sth->finish();
## add to database, and get idx.
$sth = $dbh->prepare("INSERT INTO `Value_Codes` (Table_x_Column,Item_Value) VALUES ('Variants_x_ANNOVAR_refgene_VariantType',?)");
foreach(@$aref) {
	$sth->execute($_->[0]);
	$last_insert = $dbh->last_insert_id(undef,undef, undef,undef);
	#print "$last_insert => $_->[0]\n";
	$rghash{$_->[0]} = $last_insert;
}
## replace in database.
$sth->finish();
$sth = $dbh->prepare("UPDATE `Variants_x_ANNOVAR_refgene` SET VariantType = ? WHERE VariantType = ?");
foreach(keys(%rghash)) {
	my $replace = $_;
	my $intval = $rghash{$_};
	$sth->execute($intval,$replace);
}
$sth->finish();

#############################
## 1: knownGene Location ##
#############################
my %rghash = ();
# fetch distinct values
my $sth = $dbh->prepare("SELECT DISTINCT(GeneLocation) From `Variants_x_ANNOVAR_knowngene`");
$sth->execute();
my $aref = $sth->fetchall_arrayref();
$sth->finish();
## add to database, and get idx.
$sth = $dbh->prepare("INSERT INTO `Value_Codes` (Table_x_Column,Item_Value) VALUES ('Variants_x_ANNOVAR_knowngene_GeneLocation',?)");
foreach(@$aref) {
	$sth->execute($_->[0]);
	$last_insert = $dbh->last_insert_id(undef,undef, undef,undef);
	#print "$last_insert => $_->[0]\n";
	$rghash{$_->[0]} = $last_insert;
}
## replace in database.
$sth->finish();
$sth = $dbh->prepare("UPDATE `Variants_x_ANNOVAR_knowngene` SET GeneLocation = ? WHERE GeneLocation = ?");
foreach(keys(%rghash)) {
	my $replace = $_;
	my $intval = $rghash{$_};
	$sth->execute($intval,$replace);
}
$sth->finish();
#############################
## 1: knownGene VariantType ##
#############################
%rghash = ();
# fetch distinct values
my $sth = $dbh->prepare("SELECT DISTINCT(VariantType) From `Variants_x_ANNOVAR_knowngene`");
$sth->execute();
my $aref = $sth->fetchall_arrayref();
$sth->finish();
## add to database, and get idx.
$sth = $dbh->prepare("INSERT INTO `Value_Codes` (Table_x_Column,Item_Value) VALUES ('Variants_x_ANNOVAR_knowngene_VariantType',?)");
foreach(@$aref) {
	$sth->execute($_->[0]);
	$last_insert = $dbh->last_insert_id(undef,undef, undef,undef);
	#print "$last_insert => $_->[0]\n";
	$rghash{$_->[0]} = $last_insert;
}
## replace in database.
$sth->finish();
$sth = $dbh->prepare("UPDATE `Variants_x_ANNOVAR_knowngene` SET VariantType = ? WHERE VariantType = ?");
foreach(keys(%rghash)) {
	my $replace = $_;
	my $intval = $rghash{$_};
	$sth->execute($intval,$replace);
}
$sth->finish();

#############################
## 1: ensGene Location ##
#############################
my %rghash = ();
# fetch distinct values
my $sth = $dbh->prepare("SELECT DISTINCT(GeneLocation) From `Variants_x_ANNOVAR_ensgene`");
$sth->execute();
my $aref = $sth->fetchall_arrayref();
$sth->finish();
## add to database, and get idx.
$sth = $dbh->prepare("INSERT INTO `Value_Codes` (Table_x_Column,Item_Value) VALUES ('Variants_x_ANNOVAR_ensgene_GeneLocation',?)");
foreach(@$aref) {
	$sth->execute($_->[0]);
	$last_insert = $dbh->last_insert_id(undef,undef, undef,undef);
	#print "$last_insert => $_->[0]\n";
	$rghash{$_->[0]} = $last_insert;
}
## replace in database.
$sth->finish();
$sth = $dbh->prepare("UPDATE `Variants_x_ANNOVAR_ensgene` SET GeneLocation = ? WHERE GeneLocation = ?");
foreach(keys(%rghash)) {
	my $replace = $_;
	my $intval = $rghash{$_};
	$sth->execute($intval,$replace);
}
$sth->finish();
#############################
## 1: ensGene VariantType ##
#############################
%rghash = ();
# fetch distinct values
my $sth = $dbh->prepare("SELECT DISTINCT(VariantType) From `Variants_x_ANNOVAR_ensgene`");
$sth->execute();
my $aref = $sth->fetchall_arrayref();
$sth->finish();
## add to database, and get idx.
$sth = $dbh->prepare("INSERT INTO `Value_Codes` (Table_x_Column,Item_Value) VALUES ('Variants_x_ANNOVAR_ensgene_VariantType',?)");
foreach(@$aref) {
	$sth->execute($_->[0]);
	$last_insert = $dbh->last_insert_id(undef,undef, undef,undef);
	#print "$last_insert => $_->[0]\n";
	$rghash{$_->[0]} = $last_insert;
}
## replace in database.
$sth->finish();
$sth = $dbh->prepare("UPDATE `Variants_x_ANNOVAR_ensgene` SET VariantType = ? WHERE VariantType = ?");
foreach(keys(%rghash)) {
	my $replace = $_;
	my $intval = $rghash{$_};
	$sth->execute($intval,$replace);
}
$sth->finish();


#############################
## 1: SIFT VariantType ##
#############################
%rghash = ();
# fetch distinct values
my $sth = $dbh->prepare("SELECT DISTINCT(type) From `Variants_x_SIFT`");
$sth->execute();
my $aref = $sth->fetchall_arrayref();
$sth->finish();
## add to database, and get idx.
$sth = $dbh->prepare("INSERT INTO `Value_Codes` (Table_x_Column,Item_Value) VALUES ('Variants_x_SIFT_type',?)");
foreach(@$aref) {
	$sth->execute($_->[0]);
	$last_insert = $dbh->last_insert_id(undef,undef, undef,undef);
	#print "$last_insert => $_->[0]\n";
	$rghash{$_->[0]} = $last_insert;
}
## replace in database.
$sth->finish();
$sth = $dbh->prepare("UPDATE `Variants_x_SIFT` SET type = ? WHERE type = ?");
foreach(keys(%rghash)) {
	my $replace = $_;
	my $intval = $rghash{$_};
	$sth->execute($intval,$replace);
}
$sth->finish();

#############################
## 1: SNPeff Effect ##
#############################
%rghash = ();
# fetch distinct values
my $sth = $dbh->prepare("SELECT DISTINCT(Effect) From `Variants_x_snpEff_GRCh37.66`");
$sth->execute();
my $aref = $sth->fetchall_arrayref();
$sth->finish();
## add to database, and get idx.
$sth = $dbh->prepare("INSERT INTO `Value_Codes` (Table_x_Column,Item_Value) VALUES ('Variants_x_snpEff_GRCh37.66_Effect',?)");
foreach(@$aref) {
	$sth->execute($_->[0]);
	$last_insert = $dbh->last_insert_id(undef,undef, undef,undef);
	#print "$last_insert => $_->[0]\n";
	$rghash{$_->[0]} = $last_insert;
}
## replace in database.
$sth->finish();
$sth = $dbh->prepare("UPDATE `Variants_x_snpEff_GRCh37.66` SET Effect = ? WHERE Effect = ?");
foreach(keys(%rghash)) {
	my $replace = $_;
	my $intval = $rghash{$_};
	$sth->execute($intval,$replace);
}
$sth->finish();

#############################
## 1: SNPeff Effect ##
#############################
%rghash = ();
# fetch distinct values
my $sth = $dbh->prepare("SELECT DISTINCT(EffectImpact) From `Variants_x_snpEff_GRCh37.66`");
$sth->execute();
my $aref = $sth->fetchall_arrayref();
$sth->finish();
## add to database, and get idx.
$sth = $dbh->prepare("INSERT INTO `Value_Codes` (Table_x_Column,Item_Value) VALUES ('Variants_x_snpEff_GRCh37.66_EffectImpact',?)");
foreach(@$aref) {
	$sth->execute($_->[0]);
	$last_insert = $dbh->last_insert_id(undef,undef, undef,undef);
	#print "$last_insert => $_->[0]\n";
	$rghash{$_->[0]} = $last_insert;
}
## replace in database.
$sth->finish();
$sth = $dbh->prepare("UPDATE `Variants_x_snpEff_GRCh37.66` SET EffectImpact = ? WHERE EffectImpact = ?");
foreach(keys(%rghash)) {
	my $replace = $_;
	my $intval = $rghash{$_};
	$sth->execute($intval,$replace);
}
$sth->finish();

#############################
## 1: SNPeff Effect ##
#############################
%rghash = ();
# fetch distinct values
my $sth = $dbh->prepare("SELECT DISTINCT(FunctionalClass) From `Variants_x_snpEff_GRCh37.66`");
$sth->execute();
my $aref = $sth->fetchall_arrayref();
$sth->finish();
## add to database, and get idx.
$sth = $dbh->prepare("INSERT INTO `Value_Codes` (Table_x_Column,Item_Value) VALUES ('Variants_x_snpEff_GRCh37.66_FunctionalClass',?)");
foreach(@$aref) {
	$sth->execute($_->[0]);
	$last_insert = $dbh->last_insert_id(undef,undef, undef,undef);
	#print "$last_insert => $_->[0]\n";
	$rghash{$_->[0]} = $last_insert;
}
## replace in database.
$sth->finish();
$sth = $dbh->prepare("UPDATE `Variants_x_snpEff_GRCh37.66` SET FunctionalClass = ? WHERE FunctionalClass = ?");
foreach(keys(%rghash)) {
	my $replace = $_;
	my $intval = $rghash{$_};
	$sth->execute($intval,$replace);
}
$sth->finish();

#############################
## 1: SNPeff Effect ##
#############################
%rghash = ();
# fetch distinct values
my $sth = $dbh->prepare("SELECT DISTINCT(GeneCoding) From `Variants_x_snpEff_GRCh37.66`");
$sth->execute();
my $aref = $sth->fetchall_arrayref();
$sth->finish();
## add to database, and get idx.
$sth = $dbh->prepare("INSERT INTO `Value_Codes` (Table_x_Column,Item_Value) VALUES ('Variants_x_snpEff_GRCh37.66_GeneCoding',?)");
foreach(@$aref) {
	$sth->execute($_->[0]);
	$last_insert = $dbh->last_insert_id(undef,undef, undef,undef);
	#print "$last_insert => $_->[0]\n";
	$rghash{$_->[0]} = $last_insert;
}
## replace in database.
$sth->finish();
$sth = $dbh->prepare("UPDATE `Variants_x_snpEff_GRCh37.66` SET GeneCoding = ? WHERE GeneCoding = ?");
foreach(keys(%rghash)) {
	my $replace = $_;
	my $intval = $rghash{$_};
	$sth->execute($intval,$replace);
}
$sth->finish();

#############################
## 1: SNPeff Effect ##
#############################
%rghash = ();
# fetch distinct values
my $sth = $dbh->prepare("SELECT DISTINCT(TranscriptBiotype) From `Variants_x_snpEff_GRCh37.66`");
$sth->execute();
my $aref = $sth->fetchall_arrayref();
$sth->finish();
## add to database, and get idx.
$sth = $dbh->prepare("INSERT INTO `Value_Codes` (Table_x_Column,Item_Value) VALUES ('Variants_x_snpEff_GRCh37.66_TranscriptBiotype',?)");
foreach(@$aref) {
	$sth->execute($_->[0]);
	$last_insert = $dbh->last_insert_id(undef,undef, undef,undef);
	#print "$last_insert => $_->[0]\n";
	$rghash{$_->[0]} = $last_insert;
}
## replace in database.
$sth->finish();
$sth = $dbh->prepare("UPDATE `Variants_x_snpEff_GRCh37.66` SET TranscriptBiotype = ? WHERE TranscriptBiotype = ?");
foreach(keys(%rghash)) {
	my $replace = $_;
	my $intval = $rghash{$_};
	$sth->execute($intval,$replace);
}
$sth->finish();

