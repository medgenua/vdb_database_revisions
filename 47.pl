#!/usr/bin/perl

use DBI;
use Getopt::Std;

my $file = $ARGV[0];
if ($file eq '') {

        # check default cred file
        if (-e '../.Credentials/.credentials') {
                $file = '../.Credentials/.credentials';
        }
        else{
                print "Path to credentials file not provided.\n";
                print " USAGE: perl ApplyUpdates.pl /path/to/.credentials.txt\n";
                exit();
        }
}
# read in config file
my %config;
open CONFIG, $file or die "can't open $file: $!";
while (<CONFIG>) {
    chomp;
    s/#.*//; # Remove comments
    s/^\s+//; # Remove opening whitespace
    s/\s+$//;  # Remove closing whitespace
    next unless length;
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
}
close CONFIG;
$userid = $config{'DBUSER'};
$userpass = $config{'DBPASS'};
$host = $config{'DBHOST'};
$scriptdir = $config{'SCRIPTDIR'};
# disable output buffer
$|++;

# argument to skip some steps if performing initial installation.
$install = 0;
if (defined($ARGV[1]) && $ARGV[1] eq 'install') {
	$install = 1;
	# annotation job. skip.
	exit;
}


## download snp142 annotation table from UCSC
system("cd '$scriptdir/Annotations/ANNOVAR' && perl annotate_variation.pl -buildver hg19 -downdb 'snp142' humandb/ ");
## index (thanks to lottpaul on seqanswers)
my %index;
my $input_file = "$scriptdir/Annotations/ANNOVAR/humandb/hg19_snp142.txt";
my $bin_size = 1000;
my $file_size = -s $input_file;
open(my $in, "<", $input_file) or die "Couldn't open $input_file for indexing\n";
my $previous_file_position = tell $in;

while (my $ln = <$in>) {
	my ($ucscbin,$chr,$start,$stop,$remainder) = split "\t", $ln,5;
	my $bin_start = int($start/$bin_size) * $bin_size;
	my $current_file_position = tell $in;

	if (!exists $index{$chr}->{$bin_start}) {
		$index{$chr}->{$bin_start} = [$previous_file_position, $current_file_position];
	}
	else{
		$index{$chr}->{$bin_start}->[1] = $current_file_position;
	}
	$previous_file_position = $current_file_position;
}
close $in;

my $index_file = $input_file . ".idx";
open OUT, ">$index_file";
print OUT "#BIN\t$bin_size\t$file_size\n";
foreach my $chr ((1,10..19,2,20,21,22,3..9,"MT","X","Y")){ #Ordered array to match other Annovar idx files
	foreach my $index_region (sort keys %{$index{"chr".$chr}}){
		my $start	= $index{"chr".$chr}->{$index_region}->[0];
		my $stop	= $index{"chr".$chr}->{$index_region}->[1];
		print OUT "chr$chr\t$index_region\t$start\t$stop\n";
	}
}
close OUT;
## force the annotations to update.
system("echo 1 > $scriptdir/Annotations/update.txt");

## send email to admins
# notify they need to change 
#   - install php5-curl
#   - allow .htaccess overrides for api
#   - set server query_other_jobs = TRUE   : in torque qmgr.
open OUT, ">/tmp/vdb_update.email";
print OUT "from:geert.vandeweyer\@uantwerpen.be\n";
print OUT "subject:VariantDB update: Admin action required\n";
print OUT "\n";
print OUT "Dear VariantDB administrator,\r\n";
print OUT "\r\n";
print OUT "I apologise for this second email in a short timeframe, but an additional action is required after recent VariantDB upgrades. Since the annotation procedure was updated, the monitor script, located under $scriptdir/Annotations/Annotate.pl should be restarted. As noted in the installation instructions, it is recommended to start this script upon boot using the /etc/rc.local system.\r\n\r\n";
print OUT "\r\n";
print OUT "That's it. \r\n";
print OUT "\r\n";
print OUT "Geert.";
close OUT;

#########################
## Connect to database ##
######################### 
print "Connecting to database\n";

# get current genome build/database to update
my $dbhgb = DBI->connect("dbi:mysql:NGS-Variants-Admin:$host",$userid,$userpass);
my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
$gbsth->execute();
my @row = $gbsth->fetchrow_array();
$gbsth->finish();
$db = "NGS-Variants".$row[0];
$newbuild = $row[1];
$ucscbuild = $row[0];
$ucscbuild =~ s/^-//;
print "\t=> Using current build database (build ".$row[1].")\n";
$dbh = DBI->connect("dbi:mysql:$db:$host",$userid,$userpass) or die("Could not connect to database with credentials provided in $file");
$dbh->{mysql_auto_reconnect} = 1;
$dbh->{PrintError} = 0;
$dbh->{RaiseError} = 0;

$sth = $dbh->prepare("SELECT email FROM `Users` WHERE level = 3");
$sth->execute();
while(my @row = $sth->fetchrow_array()) {
	system("sendmail $row[0] < /tmp/vdb_update.email");
}
system("rm /tmp/vdb_update.email");

