DROP TABLE IF EXISTS `NGS-Variants-Admin`.`LiftOverSteps`;
CREATE TABLE  `NGS-Variants-Admin`.`LiftOverSteps` (`idx` INT( 11 ) NOT NULL , `file` VARCHAR( 255 ) NOT NULL , `Description` TEXT NOT NULL ,PRIMARY KEY (  `idx` )) ENGINE = MYISAM DEFAULT CHARSET = latin1 COMMENT =  'Description and info on Lift Steps';
INSERT IGNORE INTO  `NGS-Variants-Admin`.`LiftOverSteps` (`idx` ,`file` ,`Description`)VALUES ('1',  'LiftOver/inc_1_create_new_db.inc',  'Initialise the new database: 1/ Ask for the new genomic build; 2/ Copy Structure to new DB; 3/ fill static tables');
INSERT IGNORE INTO `NGS-Variants-Admin`.`LiftOverSteps` (`idx`, `file`, `Description`) VALUES ('2', 'LiftOver/inc_2_Annot.inc', 'Update the Annotations Tracks : 1/ Provide new table urls ; 2/ test if urls are valid; 3/ Download annotation tables'), ('3', 'LiftOver/inc_3_LiftNonCritical.inc', 'Lift Non Critical tables, keep current build active');
INSERT IGNORE INTO `NGS-Variants-Admin`.`LiftOverSteps` (`idx`, `file`, `Description`) VALUES ('4', 'LiftOver/inc_4_variants.inc', 'Lift Variants tables : SET BUILD INACTIVE; COPY all sample related data; Copy Variants tables '), ('5', 'LiftOver/inc_5_final.inc', 'List items to do manually ; send out emails to users; restore project permissions ; set new build to active.');
DROP TABLE IF EXISTS `NGS-Variants-Admin`.`LiftsInProgress`;
CREATE TABLE `NGS-Variants-Admin`.`LiftsInProgress` (  `frombuild` varchar(50) NOT NULL,  `tobuild` varchar(50) NOT NULL,  `step` varchar(4) NOT NULL,  `statictables` text NOT NULL,  PRIMARY KEY (`frombuild`,`tobuild`)) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Current status of lift processes';
DROP TABLE IF EXISTS `NGS-Variants-Admin`.`NewBuild`;
CREATE TABLE `NGS-Variants-Admin`.`NewBuild` (  `name` varchar(20) NOT NULL COMMENT 'Use UCSC style (hgXX)',  `StringName` varchar(255) NOT NULL,  `Description` text NOT NULL,  PRIMARY KEY (`name`)) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Info on future build, will be copied to CurrentBuild';
ALTER TABLE  `NGS-Variants-hg19`.`ClinVar_db` COMMENT =  'Local Version of ClinVar. Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`ClinVar_XRefs` COMMENT =  'Clinvar External References. Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`GenePanels` COMMENT =  'Gene Panel Information and ownership. G.B. Independent';
ALTER TABLE  `NGS-Variants-hg19`.`GenePanels_x_Genes` COMMENT =  'Gene Panel Genes, based on GeneID. G.B. Independent';
ALTER TABLE  `NGS-Variants-hg19`.`GenePanels_x_Users` COMMENT =  'Gene Panel Access rights. G.B. Independent';
ALTER TABLE  `NGS-Variants-hg19`.`GO_db` COMMENT =  'Local Version of Gene Ontology. Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`GO_dbxref` COMMENT =  'Gene Ontolgoy external references. Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`GO_Summary` COMMENT =  'Local Summarized Version of Gene Ontology. Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`GO_term` COMMENT =  'Local Version of Gene Ontology. Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`GO_term2term` COMMENT =  'Local Version of Gene Ontology. Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`GO_term_dbxref` COMMENT =  'Local Version of Gene Ontology. Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`HPO_Synonyms` COMMENT =  'Human Phenotype Ontology. G.B. Independent';
ALTER TABLE  `NGS-Variants-hg19`.`HPO_Terms` COMMENT =  'Human Phenotype Ontology. G.B. Independent';
ALTER TABLE  `NGS-Variants-hg19`.`HPO_Term_x_Term` COMMENT =  'HPO Term1 is_a Term2. G.B. Independent';
ALTER TABLE  `NGS-Variants-hg19`.`Log` COMMENT =  'Log Table for Variant editing. G.B. Independent';
ALTER TABLE  `NGS-Variants-hg19`.`Projects` COMMENT =  'Projects info. G.B. Independent';
ALTER TABLE  `NGS-Variants-hg19`.`Projects_x_Users` COMMENT =  'Projects user permission info. G.B. Independent';
ALTER TABLE  `NGS-Variants-hg19`.`Samples_x_HPO_Terms` COMMENT =  'Sample Phenotype Information. G.B. Independent';
DROP TABLE IF EXISTS `NGS-Variants-hg19`.`Samples_x_Phenotypes`;
ALTER TABLE  `NGS-Variants-hg19`.`Samples_x_Samples` COMMENT =  'Sample Relations. G.B. Independent';
ALTER TABLE  `NGS-Variants-hg19`.`Users_x_Annotations` COMMENT =  'Saved Annotations by Users. G.B. Independent';
ALTER TABLE  `NGS-Variants-hg19`.`Users_x_FilterSettings` COMMENT =  'Saved Filter Combinations by Users. G.B. Independent';
ALTER TABLE  `NGS-Variants-hg19`.`Users_x_Recent` COMMENT =  'Recent Queries By User. G.B. Independent';
ALTER TABLE  `NGS-Variants-hg19`.`Value_Codes` COMMENT =  'Coded Annotation Values. Reusable. G.B. Independent';
ALTER TABLE  `NGS-Variants-hg19`.`Variants` COMMENT =  'Variant info. G.B. Dependent';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_1000g2012apr_afr` COMMENT =  'Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_1000g2012apr_all` COMMENT =  'Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_1000g2012apr_amr` COMMENT =  'Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_1000g2012apr_asn` COMMENT =  'Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_1000g2012apr_eur` COMMENT =  'Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_CADD` COMMENT =  'Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_ensgene` COMMENT =  'Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_esp5400_aa` COMMENT =  'Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_esp5400_all` COMMENT =  'Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_esp5400_ea` COMMENT =  'Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_esp6500si_aa` COMMENT =  'Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_esp6500si_all` COMMENT =  'Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_esp6500si_ea` COMMENT =  'Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_knowngene` COMMENT =  'Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_ljb_gerp` COMMENT =  'Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_ljb_lrt` COMMENT =  'Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_ljb_mt` COMMENT =  'Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_ljb_phylop` COMMENT =  'Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_ljb_pp2` COMMENT =  'Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_ljb_sift` COMMENT =  'Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_refgene` COMMENT =  'Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_rmsk` COMMENT =  'Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_segdup` COMMENT =  'Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_snp130` COMMENT =  'Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_snp135` COMMENT =  'Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_snp137` COMMENT =  'Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ClinVar` COMMENT =  'Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_GenePanels` COMMENT =  'Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_GO` COMMENT =  'Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_MutationTaster` COMMENT =  'Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_Samples` COMMENT =  'Table Linking Variants To Samples. G.B. Independent';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_SIFT` COMMENT =  'Annotation Table';
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_snpEff_GRCh37.66` COMMENT =  'Annotation Table';
DROP TABLE IF EXISTS `NGS-Variants-hg19`.`Annotation_DataValues`;
CREATE TABLE  `NGS-Variants-hg19`.`Annotation_DataValues` (`Annotation` VARCHAR( 255 ) NOT NULL ,`Value` VARCHAR( 255 ) NOT NULL ,PRIMARY KEY (  `Annotation` )) ENGINE = MYISAM COMMENT =  'Replacements values for anno scripts. G.B. Dependent';
DROP TABLE IF EXISTS  `NGS-Variants-hg19`.`Variants_x_snpEff`;
RENAME TABLE  `NGS-Variants-hg19`.`Variants_x_snpEff_GRCh37.66` TO  `NGS-Variants-hg19`.`Variants_x_snpEff` ;
INSERT IGNORE INTO  `NGS-Variants-hg19`.`Annotation_DataValues` (`Annotation` ,`Value`)VALUES ('snpEff',  'GRCh37.66'), ('ANNOVAR',  'hg19');
ALTER TABLE  `NGS-Variants-hg19`.`Annotation_DataValues` ADD  `Information` TEXT NOT NULL;
UPDATE  `NGS-Variants-hg19`.`Annotation_DataValues` SET  `Information` =  'Definition of the snpEff annotation file. This is used as the -v parameter.  It is typically an Ensembl build plus version. ' WHERE  `Annotation_DataValues`.`Annotation` =  'snpEff';
UPDATE  `NGS-Variants-hg19`.`Annotation_DataValues` SET  `Information` =  'Annovar table version. It is used as the --buildver parameter and is typically a UCSC genome version.' WHERE  `Annotation_DataValues`.`Annotation` =  'ANNOVAR';
DROP TABLE IF EXISTS `NGS-Variants-Admin`.`FailedLifts`;
CREATE TABLE `NGS-Variants-Admin`.`FailedLifts` ( `frombuild` varchar(20) NOT NULL, `tobuild` varchar(20) NOT NULL, `tablename` varchar(50) NOT NULL, `itemID` int(11) NOT NULL, `ID` int(11) NOT NULL AUTO_INCREMENT, `Reason` varchar(255) NOT NULL, PRIMARY KEY (`ID`), KEY `table` (`tablename`), KEY `tobuild` (`tobuild`)) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Region & table IDs that failed to lift ';
ALTER TABLE   `NGS-Variants-Admin`.`FailedLifts` ADD INDEX (  `frombuild` );
ALTER TABLE  `NGS-Variants-Admin`.`FailedLifts` ADD INDEX (  `itemID` );
DELETE FROM `NGS-Variants-Admin`.`LiftOverSteps` WHERE `LiftOverSteps`.`idx` = 4;
UPDATE  `NGS-Variants-Admin`.`LiftOverSteps` SET  `idx` =  '4',`file` =  'LiftOver/inc_4_final.inc' WHERE  `LiftOverSteps`.`idx` =5;
ALTER TABLE  `NGS-Variants-hg19`.`Log` ADD INDEX (  `arguments` );
DROP TABLE IF EXISTS `NGS-Variants-Admin`.`PreviousBuilds`;
CREATE TABLE `NGS-Variants-Admin`.`PreviousBuilds` ( `name` varchar(20) NOT NULL COMMENT 'Use UCSC style (hgXX)', `StringName` varchar(255) NOT NULL, `Description` text NOT NULL, PRIMARY KEY (`name`)) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Info on old builds, for archive browsing';
DROP TABLE IF EXISTS `NGS-Variants-hg19`.`Archived_Annotations`;
CREATE TABLE  `NGS-Variants-hg19`.`Archived_Annotations` (`aid` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,`vid` BIGINT NOT NULL ,`SourceTable` INT NOT NULL ,`SourceBuild` INT NOT NULL ,`ArchiveDate` DATE NOT NULL , `Contents` TEXT NOT NULL) ENGINE = MYISAM COMMENT =  'Replaced Annotations For BackTracking';
ALTER TABLE  `NGS-Variants-hg19`.`Archived_Annotations` ADD INDEX (  `vid` );
ALTER TABLE  `NGS-Variants-hg19`.`Archived_Annotations` ADD INDEX (  `SourceTable` );
ALTER TABLE  `NGS-Variants-hg19`.`Archived_Annotations` ADD INDEX (  `SourceBuild` );
ALTER TABLE  `NGS-Variants-hg19`.`Archived_Annotations` ADD INDEX (  `ArchiveDate` );
ALTER TABLE  `NGS-Variants-hg19`.`Archived_Annotations` CHANGE  `SourceTable`  `SourceTable` VARCHAR( 75 ) NOT NULL ,CHANGE  `SourceBuild`  `SourceBuild` VARCHAR( 10 ) NOT NULL;
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_ensgene` CHANGE  `GeneSymbol`  `GeneSymbol` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_knowngene` CHANGE  `GeneSymbol`  `GeneSymbol` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_refgene` CHANGE  `GeneSymbol`  `GeneSymbol` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_ensgene` CHANGE  `CPointNT`  `CPointNT` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,CHANGE  `CPointAA`  `CPointAA` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_knowngene` CHANGE  `CPointNT`  `CPointNT` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,CHANGE  `CPointAA`  `CPointAA` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_refgene` CHANGE  `CPointNT`  `CPointNT` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,CHANGE  `CPointAA`  `CPointAA` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
INSERT IGNORE INTO `NGS-Variants-hg19`.`Annotation_DataValues` (`Annotation`, `Value`, `Information`) VALUES ('IGV', 'hg19', 'Genome key for IGV, used for fetching missing depths on VCF import. Located in scriptdir/IGVTools_bin/genomes/<Value>.chrom.sizes');
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_snp135` DROP  `other` ;
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_snp137` DROP  `other` ;
TRUNCATE TABLE `NGS-Variants-hg19`.`Variants_x_ClinVar`;
ALTER TABLE   `NGS-Variants-hg19`.`Variants_x_ClinVar` DROP  `GeneXRef`;
ALTER TABLE  `NGS-Variants-hg19`.`ClinVar_db` CHANGE  `id`  `id` VARCHAR( 14 ) NOT NULL;
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ClinVar` CHANGE  `cvid`  `cvid` VARCHAR( 14 ) NOT NULL;
ALTER TABLE   `NGS-Variants-hg19`.`Variants_x_ClinVar` ADD INDEX (  `cvid` );
ALTER TABLE   `NGS-Variants-hg19`.`Variants_x_ClinVar` CHANGE  `NP_ID`  `NP_ID` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
