#!/usr/bin/perl

################
# LOAD MODULES #
################
use DBI;
use Getopt::Std;

my $file = $ARGV[0];
if ($file eq '') {
    # check default cred file
    if (-e '../.Credentials/.credentials') {
        $file = '../.Credentials/.credentials';
    }
    else {
	print "Path to credentials file not provided.\n";
	print " USAGE: perl ApplyUpdates.pl /path/to/.credentials.txt\n";
        exit();
    }
}

# read in config file
my %config;
open CONFIG, $file or die "can't open $file: $!";
while (<CONFIG>) {
    chomp;
    s/#.*//; # Remove comments
    s/^\s+//; # Remove opening whitespace
    s/\s+$//;  # Remove closing whitespace
    next unless length;
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
}
close CONFIG;
my $userid = $config{'DBUSER'};
my $userpass = $config{'DBPASS'};
my $host = $config{'DBHOST'};

# argument to skip some steps if performing initial installation.
$install = 0;
if (defined($ARGV[1]) && $ARGV[1] eq 'install') {
	$install = 1;
	my $paths = `hg paths`;
	if ($paths =~ m/bitbucket/) {
		# already cloned from bitbucket. skip
		exit;
	}
}



#########################
## Connect to database ##
######################### 
print "Connecting to database\n";

# get current genome build/database to update
my $dbhgb = DBI->connect("dbi:mysql:NGS-Variants-Admin:$host",$userid,$userpass);
my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
$gbsth->execute();
my @row = $gbsth->fetchrow_array();
$gbsth->finish();
$db = "NGS-Variants".$row[0];
$newbuild = $row[1];
$ucscbuild = $row[0];
$ucscbuild =~ s/^-//;
print "\t=> Using current build database (build ".$row[1].")\n";
$dbh = DBI->connect("dbi:mysql:$db:$host",$userid,$userpass) or die("Could not connect to database with credentials provided in $file");
$dbh->{mysql_auto_reconnect} = 1;
$dbh->{PrintError} = 0;
$dbh->{RaiseError} = 0;

# AIMs
#  1. notify admin users of migration to bitbucket
#  2. provide them with instructions on how to perform the migration.

my $sth =$dbh->prepare("SELECT email FROM `Users` WHERE level = 3");
$sth->execute();

my $result = $sth->fetchall_arrayref();
$sth->finish();

my $admins;
foreach my $line (@$result) {
    my $admin = $line->[0];
    $admins .= "$admin,";
}

$admins = substr($admins,0,-1);
my $mailfile = `mktemp`;

open OUT, ">$mailfile";
print OUT "BCC: geert.vandeweyer\@uantwerpen.be\n";
print OUT "To: $admins\n";
print OUT "subject: VariantDB source changed\n";
print OUT "from: geert.vandeweyer\@uantwerpen.be\n\n";
print OUT "Dear,\r\n\r\n";
print OUT "You received this e-mail because you are the administrator of a local VariantDB installation. We have moved our code base to BitBucket.\r\n";
print OUT "In order to be get future updates, you need to change some settings in your VariantDB installation files. ";
print OUT "Please follow these steps:\r\n\r\n";
print OUT "1) Login via SSH\r\n\r\n";
print OUT "2) Go to your VariantDB installation folder (default is /VariantDB). This folder will contain several of these subfolders:\r\n";
print OUT "\t- Web-Interface\r\n";
print OUT "\t- .Credentials (hidden folder)\r\n";
print OUT "\t- Database_Revisions\r\n";
print OUT "\t- Installation_Scripts\r\n";
print OUT "3) In each of these subfolders, you will find a file 'hgrc' located in the hidden subdirectory '.hg/' (example: Web-Interface/.hg/hgrc)\r\n";
print OUT "     There should a line specifying the code base URL (for example, for Web-Interface):\r\n";
print OUT "     default = http://cnv-webstore.ua.ac.be/hg/VariantDB/Web-Interface\r\n\r\n";
print OUT "4) Change the URLs like this\r\n";
print OUT "     http://cnv-webstore.ua.ac.be/hg/VariantDB/Web-Interface --> https://bitbucket.org/medgenua/vdb_web-interface\r\n";
print OUT "     http://cnv-webstore.ua.ac.be/hg/VariantDB/Database_Revisions --> https://bitbucket.org/medgenua/vdb_database_revisions\r\n";
print OUT "     http://cnv-webstore.ua.ac.be/hg/VariantDB/.Credentials --> https://bitbucket.org/medgenua/vdb_credentials\r\n";
print OUT "     http://cnv-webstore.ua.ac.be/hg/VariantDB/Installation_Scripts --> https://bitbucket.org/medgenua/vdb_installation_scripts\r\n\r\n";
print OUT "To verify the changes work, go to your VariantDB platform and get the latest updates (for the top menu: Settings --> Platform Updates).\r\n";
print OUT "You should see updates for web-interface, database-revisions and credentials. Apply the changes and you will receive another email to confirm the changes were succesful. ";
print OUT "If you don't receive an e-mail or someting else went wrong, please contact me for further help\r\n\r\n\r\n";
print OUT "Kind regards,\r\nGeert\r\n\r\n";
close OUT;
## send mail
system("sendmail -t < $mailfile");
system("rm $mailfile");
