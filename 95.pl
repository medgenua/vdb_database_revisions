#!/usr/bin/perl

use DBI;
use Getopt::Std;

my $file = $ARGV[0];
if ($file eq '') {

	# check default cred file
	if (-e '../.Credentials/.credentials') {
		$file = '../.Credentials/.credentials';
	}
	else{
		print "Path to credentials file not provided.\n";
		print " USAGE: perl ApplyUpdates.pl /path/to/.credentials.txt\n";
		exit();
	}
}
# read in config file
my %config;
open CONFIG, $file or die "can't open $file: $!";
while (<CONFIG>) {
    chomp;
    s/#.*//; # Remove comments
    s/^\s+//; # Remove opening whitespace
    s/\s+$//;  # Remove closing whitespace
    next unless length;
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
}
close CONFIG;
$userid = $config{'DBUSER'};
$userpass = $config{'DBPASS'};
$host = $config{'DBHOST'};
$datadir = $config{'DATADIR'};
$scriptpass = $config{'SCRIPTPASS'};
$scriptuser = $config{'SCRIPTUSER'};
$scriptdir = $config{'SCRIPTDIR'};
# disable output buffer
$|++;


#########################
## Connect to database ##
######################### 
print "Connecting to database\n";

# get current genome build/database to update
my $dbhgb = DBI->connect("dbi:mysql:NGS-Variants-Admin:$host",$userid,$userpass);
my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
$gbsth->execute();
my @row = $gbsth->fetchrow_array();
$gbsth->finish();
$db = "NGS-Variants".$row[0];
$newbuild = $row[1];
$ucscbuild = $row[0];
$ucscbuild =~ s/^-//;
print "\t=> Using current build database (build ".$row[1].")\n";
$dbh = DBI->connect("dbi:mysql:$db:$host",$userid,$userpass) or die("Could not connect to database with credentials provided in $file");
$dbh->{mysql_auto_reconnect} = 1;
$dbh->{PrintError} = 0;
$dbh->{RaiseError} = 0;

##########
### AIM ##
##########
# fix discrepancy between refGene and refLink tables
#  - overwrite gene symbols in refLink with values from refGene, based on NM_ids, and vice versa
#  - list genes with symbol discrepancies in database. 

my $table_prefix = "";
my $hits = 0;
my %replaced = ();
## update namings (novel symbols)
# read in refLink. 
print "$scriptdir/Annotations/ANNOVAR/$table_prefix"."humandb/$ucscbuild"."_refLink.txt\n";
open IN, "$scriptdir/Annotations/ANNOVAR/$table_prefix"."humandb/$ucscbuild"."_refLink.txt" or die("Could not open refLink for reading\n");
my %tr_x_gs = ();
my %gid_x_tr = ();
while (my $line = <IN>) {
	chomp($line);
	my @cols = split(/\t/,$line);
	$tr_x_gs{$cols[2]}{'s'} = $cols[0];
	$tr_x_gs{$cols[2]}{'i'} = $cols[6];
	$gid_x_tr{$cols[6]}{$cols[2]} = 0;
}
close IN;
## update refGene.
open IN, "$scriptdir/Annotations/ANNOVAR/$table_prefix"."humandb/$ucscbuild"."_refGene.txt" or die("Could not open refGene for reading\n"); 
open OUT, ">$scriptdir/Annotations/ANNOVAR/$table_prefix"."humandb/$ucscbuild"."_refGene.tmp.txt" or die("Could not open tmp for writing\n");
my $isth = $dbh->prepare("REPLACE INTO `GeneName_Discrepancies` (`id`,`refGene`,`refLink`,`transcript`) VALUES (?,?,?,?)");
my %gene_IDs = (); # which geneID were seen in RefGene (are the human ones).
my %back_replace = (); # to replace in refLink... 
while (my $line = <IN>) {
	chomp($line);
	my @cols = split(/\t/,$line);
	if (defined($tr_x_gs{$cols[1]})) {
		# mark transcript/geneID as seen
		$gid_x_tr{$tr_x_gs{$cols[1]}{i}}{$cols[1]} = 1;
		$gene_IDs{$tr_x_gs{$cols[1]}{i}}{'n'} = $tr_x_gs{$cols[1]}{s};
		$gene_IDs{$tr_x_gs{$cols[1]}{i}}{'o'} = $cols[12];
		# mismatch? 
		if ($tr_x_gs{$cols[1]}{'s'} ne $cols[12]) {
			$hits++; # a mismatch (multiple per symbol due to transcripts.
			# list replaced items for next step
			$replaced{$cols[12]}{s} = $tr_x_gs{$cols[1]}{s};
			$replaced{$cols[12]}{i} = $tr_x_gs{$cols[1]}{i};
			# insert it as a mismatch into database
			$isth->execute($tr_x_gs{$cols[1]}{i},$cols[12],$tr_x_gs{$cols[1]}{s},$cols[1]);
			# store old symbol
			$back_replace{$tr_x_gs{$cols[1]}{i}."|".$cols[1]} = $cols[12];
			# update the gene symbol in refGene.tmp
			$cols[12] = $tr_x_gs{$cols[1]}{s};
			
		}
	}
	else {
		$isth->execute(-1,$cols[12],-1,$cols[1]);
		#print "Transcript/Gene not in refLink : $cols[1] / $cols[12]\n";
	}
	# print (updated) entry;
	print OUT join("\t",@cols)."\n";
}
close OUT;
## update RefLink.tmp.
open IN, "$scriptdir/Annotations/ANNOVAR/$table_prefix"."humandb/$ucscbuild"."_refLink.txt" or die("Could not open refLink for reading\n");
open OUT, ">$scriptdir/Annotations/ANNOVAR/$table_prefix"."humandb/$ucscbuild"."_refLink.tmp.txt" or die("Could not open refLink.tmp for wring \n");
while (my $line = <IN>) {
	chomp($line);
	my @cols = split(/\t/,$line);
	# nm on 2 ; id on 6 and symbol on 0
	# update if seen in refGene
	if (defined($back_replace{$cols[6]."|".$cols[2]})) {
		$cols[0] = $back_replace{$cols[6]."|".$cols[2]};
	}
	print OUT join("\t",@cols)."\n";
}
close IN;
close OUT;


## go over gid_x_tr and check if there are "new transcripts" (value == 0, not seen in refGene)
my %novel = ();
foreach(sort {$a cmp $b} keys(%gid_x_tr)) {
	my $gene_ID = $_;
	next if (!defined($gene_IDs{$gene_ID}));
	my $old_symbol = $gene_IDs{$gene_ID}{'o'};
	my $new_symbol = $gene_IDs{$gene_ID}{'n'};
	# transcripts
	foreach(keys(%{$gid_x_tr{$gene_ID}})) {
		my $nm = $_;
		if ($gid_x_tr{$gene_ID}{$nm} == 0) {
			$isth->execute($gene_ID,$old_symbol,$new_symbol,$nm);
			print "NOVEL transcript for existing Gene: $gene_IDs{$gene_ID} : $gene_ID : $nm \n";

		}
	}
}
# note : truly novel genes (new geneID) are not picked up here. 

$isth->finish();
# read in genepanesl 
my $rc = $dbh->selectall_arrayref("SELECT gp.Name, gpg.gid, gpg.Symbol FROM `GenePanels` gp JOIN `GenePanels_x_Genes` gpg ON  gp.id = gpg.gpid");

my %gp = ();
foreach my $r (@$rc) {
	#my ($gpid,$gid,$symbol,$comments) = split(/,/,$_,4);
	my ($gpName,$gid,$symbol) = @$r;
	$gp{"$symbol|$gid"} .= "$gpName ; ";
}
print "Nr hits : $hits\n";
print "Unique Genes : ".keys(%replaced)."\n";
foreach(sort {$a cmp $b} keys(%replaced)) {
	if (defined($gp{$_."|".$replaced{$_}{i}})) {
		print "$_ in panels under OLD Name : ".$gp{$_."|".$replaced{$_}{i}}."\n";
	}
	if (defined($gp{$replaced{$_}{s}."|".$replaced{$_}{i}})) {
		print "$_ in panels under NEW Name (".$replaced{$_}{s}.") : ".$gp{$replaced{$_}{s}."|".$replaced{$_}{i}}."\n";
	}
	#print "$_\t$replaced{$_}{s}\t$replaced{$_}{i}\n";
}
