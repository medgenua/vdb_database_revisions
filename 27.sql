DROP TABLE IF EXISTS `NGS-Variants-hg19`.`GenePanels`;
CREATE TABLE  `NGS-Variants-hg19`.`GenePanels` (`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,`Name` VARCHAR( 255 ) NOT NULL ,`Description` TEXT NOT NULL ,`Owner` INT NOT NULL ,`Public` TINYINT( 1 ) NOT NULL DEFAULT  '0',`LastEdit` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,INDEX (  `Owner` )) ENGINE = MYISAM ;
DROP TABLE IF EXISTS `NGS-Variants-hg19`.`GenePanels_x_Users`;
CREATE TABLE  `NGS-Variants-hg19`.`GenePanels_x_Users` (`gpid` INT NOT NULL ,`uid` INT NOT NULL ,`rw` TINYINT( 1 ) NOT NULL DEFAULT  '0' COMMENT  'read/write (has permission to edit)') ENGINE = MYISAM ;
DROP TABLE IF EXISTS `NGS-Variants-hg19`.`GenePanels_x_Genes`;
CREATE TABLE  `NGS-Variants-hg19`.`GenePanels_x_Genes` (`gpid` INT NOT NULL ,`gid` INT NOT NULL ,`Symbol` VARCHAR( 255 ) NOT NULL ,`Comment` TEXT NOT NULL ,`LastEdit` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,PRIMARY KEY (  `gpid` ,  `gid` ) ,INDEX (  `Symbol` )) ENGINE = MYISAM ;
ALTER TABLE  `NGS-Variants-hg19`.`GenePanels_x_Users` ADD PRIMARY KEY (  `gpid` ,  `uid` ) ;
ALTER TABLE  `NGS-Variants-hg19`.`GenePanels_x_Users` ADD  `share` TINYINT( 1 ) NOT NULL DEFAULT  '0' COMMENT  'has permission to share';
DROP TABLE IF EXISTS `NGS-Variants-hg19`.`Variants_x_GenePanels`;
CREATE TABLE  `NGS-Variants-hg19`.`Variants_x_GenePanels` (`aid` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,`vid` BIGINT NOT NULL COMMENT  'Variant ID',`gpid` INT NOT NULL COMMENT  'GenePanel ID',`gene` VARCHAR(255) NOT NULL COMMENT  'Gene Symbol',`LastEdit` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP) ENGINE = MYISAM ;
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_GenePanels` ADD INDEX (  `vid` );
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_GenePanels` ADD INDEX (  `gpid` );
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_GenePanels` ADD INDEX (  `gene` );
