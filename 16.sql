DROP TABLE IF EXISTS `NGS-Variants-hg19`.`Users_x_Recent`;
CREATE TABLE  `NGS-Variants-hg19`.`Users_x_Recent` (`uid` INT NOT NULL ,`QueryHash` VARCHAR( 255 ) NOT NULL ,`Query` TEXT NOT NULL ,`QueryTables` TEXT NOT NULL ,`LastUsed` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,PRIMARY KEY (  `uid` ,  `QueryHash` ) ,INDEX (  `LastUsed` )) ENGINE = MYISAM ;
