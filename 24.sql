ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_ensgene` CHANGE  `GeneLocation`  `GeneLocation` SMALLINT NOT NULL ,CHANGE  `VariantType`  `VariantType` SMALLINT NOT NULL
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_knowngene` CHANGE  `GeneLocation`  `GeneLocation` SMALLINT NOT NULL ,CHANGE  `VariantType`  `VariantType` SMALLINT NOT NULL
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ANNOVAR_refgene` CHANGE  `GeneLocation`  `GeneLocation` SMALLINT NOT NULL ,CHANGE  `VariantType`  `VariantType` SMALLINT NOT NULL
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_SIFT` CHANGE  `type`  `type` SMALLINT NOT NULL
ALTER TABLE  `Variants_x_snpEff_GRCh37.66` CHANGE  `Effect`  `Effect` SMALLINT NOT NULL ,CHANGE  `EffectImpact`  `EffectImpact` SMALLINT NOT NULL ,CHANGE  `FunctionalClass`  `FunctionalClass` SMALLINT NOT NULL ,CHANGE  `GeneCoding`  `GeneCoding` SMALLINT NOT NULL ,CHANGE  `TranscriptBiotype`  `TranscriptBiotype` SMALLINT NOT NULL
