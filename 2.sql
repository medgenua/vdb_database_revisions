DROP TABLE IF EXISTS `NGS-Variants-hg19`.`HPO_Terms`;
CREATE TABLE  `NGS-Variants-hg19`.`HPO_Terms` (`id` MEDIUMINT NOT NULL COMMENT  'TermID, last part of HPO term',`Name` VARCHAR( 255 ) NOT NULL ,`Definition` TEXT NOT NULL ,`Comment` TEXT NOT NULL ,`xref` TEXT NOT NULL ,PRIMARY KEY (  `id` )) ENGINE = MYISAM ;
ALTER TABLE  `NGS-Variants-hg19`.`HPO_Terms` ADD INDEX (  `Name` )
DROP TABLE IF EXISTS  `NGS-Variants-hg19`.`HPO_Synonyms` ;
CREATE TABLE  `NGS-Variants-hg19`.`HPO_Synonyms` (`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,`tid` MEDIUMINT NOT NULL ,`Synonym` VARCHAR( 255 ) NOT NULL) ENGINE = MYISAM ;
ALTER TABLE  `NGS-Variants-hg19`.`HPO_Synonyms` ADD INDEX (  `tid` )
ALTER TABLE  `NGS-Variants-hg19`.`HPO_Synonyms` ADD INDEX (  `Synonym` )
DROP TABLE IF EXISTS `NGS-Variants-hg19`.`HPO_Term_x_Term`;
CREATE TABLE  `NGS-Variants-hg19`.`HPO_Term_x_Term` (`tid1` MEDIUMINT NOT NULL ,`tid2` MEDIUMINT NOT NULL ,PRIMARY KEY (  `tid1` ,  `tid2` )) ENGINE = MYISAM COMMENT =  'Term1 is_a Term2';
DROP TABLE IF EXISTS `NGS-Variants-hg19`.`Samples_x_HPO_Terms`;
CREATE TABLE  `NGS-Variants-hg19`.`Samples_x_HPO_Terms` (`sid` INT NOT NULL ,`tid` INT NOT NULL ,PRIMARY KEY (  `sid` ,  `tid` )) ENGINE = MYISAM ;
