#!/usr/bin/perl

use DBI;
use Getopt::Std;
my $file = $ARGV[0];
if ($file eq '') {
	# check default cred file
	if (-e '../.Credentials/.credentials') {
		$file = '../.Credentials/.credentials';
	}
	else {
		print "Path to credentials file not provided.\n";
		print " USAGE: perl ApplyUpdates.pl /path/to/.credentials.txt\n";
		exit();
	}
}
# read in config file
my %config;
open CONFIG, $file or die "can't open $file: $!";
while (<CONFIG>) {
    chomp;
    s/#.*//; # Remove comments
    s/^\s+//; # Remove opening whitespace
    s/\s+$//;  # Remove closing whitespace
    next unless length;
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
}
close CONFIG;
$userid = $config{'DBUSER'};
$userpass = $config{'DBPASS'};
$host = $config{'DBHOST'};
# disable output buffer
$|++;

# argument to skip some steps if performing initial installation.
$install = 0;
if (defined($ARGV[1]) && $ARGV[1] eq 'install') {
	$install = 1;
}

#########################
## Connect to database ##
######################### 
print "Connecting to database\n";

# get current genome build/database to update
my $dbhgb = DBI->connect("dbi:mysql:NGS-Variants-Admin:$host",$userid,$userpass);
my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
$gbsth->execute();
my @row = $gbsth->fetchrow_array();
$gbsth->finish();
$db = "NGS-Variants".$row[0];
$newbuild = $row[1];
$ucscbuild = $row[0];
$ucscbuild =~ s/^-//;
print "\t=> Using current build database (build ".$row[1].")\n";
$dbh = DBI->connect("dbi:mysql:$db:$host",$userid,$userpass) or die("Could not connect to database with credentials provided in $file");
$dbh->{mysql_auto_reconnect} = 1;
$dbh->{PrintError} = 0;
$dbh->{RaiseError} = 0;


#AIM 
# scan 'validation_comments' to identify autoclassified variants and put that into a new database field.

# process all with a class (simplest)
my $sth = $dbh->prepare("SELECT `vid`,`validationdetails`,`sid` FROM `Variants_x_Samples` WHERE class > 0");
my $nrr = $sth->execute();
my $rowcache;
print "Nr of variants to evaluate: $nrr\n";
$nr_set = 0;
while (my $r = shift(@$rowcache) || shift( @{$rowcache = $sth->fetchall_arrayref()|| []})) {
        my ($vid,$comment,$sid) = @$r; #$r->[0];
	#my $comment = $r->[1];
	if ($comment =~ m/^Auto-Classified/) {
		$dbh->do("UPDATE `Variants_x_Samples` SET `autoclassified` = 1 WHERE vid = '$vid' AND sid = '$sid'");
		$nr_set++;
		#push(@vids,$vid);
	}
	#if (scalar(@vids) == 1000) {
	#	$nr_set += 1000;
	#	$dbh->do("UPDATE `Variants_x_Samples` SET `autoclassified` = 1 WHERE vid IN (".join(",",@vids).")");
	#	@vids = ();
	#}
}
#if (scalar(@vids) > 0) {
#	$nr_set += scalar(@vids);
#	$dbh->do("UPDATE `Variants_x_Samples` SET `autoclassified` = 1 WHERE vid IN (".join(",",@vids).")");
#}
print "Number of autoclassified variants found : $nr_set\n";

	

