ALTER TABLE `NGS-Variants-hg19`.`Variants_x_ANNOVAR_refgene` ADD INDEX (  `Transcript` );
TRUNCATE TABLE `NGS-Variants-hg19`.`Variants_x_ClinVar`;
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_ClinVar` ADD  `AA_MatchType` INT( 11 ) NOT NULL ,ADD INDEX (  `AA_MatchType` );
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_Samples` ADD  `RefBaseQual` FLOAT( 3, 1 ) NOT NULL DEFAULT  '0',ADD  `AltBaseQual` FLOAT( 3, 1 ) NOT NULL DEFAULT  '0',ADD  `PhredFisherPValue` FLOAT( 5, 4 ) NOT NULL DEFAULT  '0';
