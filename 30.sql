DROP TABLE IF EXISTS `NGS-Variants-hg19`.`Variants_x_CADD`;
DROP TABLE IF EXISTS `NGS-Variants-hg19`.`Variants_x_ANNOVAR_CADD`;
CREATE TABLE  `NGS-Variants-hg19`.`Variants_x_CADD` (`aid` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,`vid` BIGINT NOT NULL ,`Raw_Score` DECIMAL( 9, 6 ) NOT NULL ,`Phred_Score` DECIMAL( 4, 1 ) NOT NULL ,INDEX (  `vid` )) ENGINE = MYISAM ;
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_CADD` ADD INDEX (  `Raw_Score` );
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_CADD` ADD INDEX (  `Phred_Score` );
RENAME TABLE `NGS-Variants-hg19`.`Variants_x_CADD` TO `NGS-Variants-hg19`.`Variants_x_ANNOVAR_CADD`;
