#!/usr/bin/perl

use DBI;
use Getopt::Std;

my $file = $ARGV[0];
if ($file eq '') {

        # check default cred file
        if (-e '../.Credentials/.credentials') {
                $file = '../.Credentials/.credentials';
        }
        else{
                print "Path to credentials file not provided.\n";
                print " USAGE: perl ApplyUpdates.pl /path/to/.credentials.txt\n";
                exit();
        }
}
# read in config file
my %config;
open CONFIG, $file or die "can't open $file: $!";
while (<CONFIG>) {
    chomp;
    s/#.*//; # Remove comments
    s/^\s+//; # Remove opening whitespace
    s/\s+$//;  # Remove closing whitespace
    next unless length;
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
}
close CONFIG;
$userid = $config{'DBUSER'};
$userpass = $config{'DBPASS'};
$host = $config{'DBHOST'};
$scriptdir = $config{'SCRIPTDIR'};
$ftpdir = $config{'FTP_DATA'};
# disable output buffer
$|++;


## create folders & set permissions of locations missing after fresh installation.

system("chmod 777 '$ftpdir'");
system("touch '$scriptdir/api/query_results/.job_queue'");
system("chmod 777 '$scriptdir/api/query_results/.job_queue'");

