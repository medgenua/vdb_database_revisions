#!/usr/bin/perl

use DBI;
use Getopt::Std;

my $file = $ARGV[0];
if ($file eq '') {

        # check default cred file
        if (-e '../.Credentials/.credentials') {
                $file = '../.Credentials/.credentials';
        }
        else{
                print "Path to credentials file not provided.\n";
                print " USAGE: perl ApplyUpdates.pl /path/to/.credentials.txt\n";
                exit();
        }
}
# read in config file
my %config;
open CONFIG, $file or die "can't open $file: $!";
while (<CONFIG>) {
    chomp;
    s/#.*//; # Remove comments
    s/^\s+//; # Remove opening whitespace
    s/\s+$//;  # Remove closing whitespace
    next unless length;
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
}
close CONFIG;
$userid = $config{'DBUSER'};
$userpass = $config{'DBPASS'};
$host = $config{'DBHOST'};
$scriptdir = $config{'SCRIPTDIR'};
# disable output buffer
$|++;

# argument to skip some steps if performing initial installation.
$install = 0;
if (defined($ARGV[1]) && $ARGV[1] eq 'install') {
	$install = 1;
	# annotation jobs. skipped
	# information on api, included in installer.
	exit;
}


## download annotation tables.
system("cd '$scriptdir/Annotations/ANNOVAR' && perl annotate_variation.pl -buildver hg19 -downdb -webfrom annovar 'avsnp138' humandb/ ");
system("cd '$scriptdir/Annotations/ANNOVAR' && perl annotate_variation.pl -buildver hg19 -downdb -webfrom annovar 'avsnp142' humandb/ ");
system("cd '$scriptdir/Annotations/ANNOVAR' && perl annotate_variation.pl -buildver hg19 -downdb -webfrom annovar '1000g2014oct' humandb/ ");
system("cd '$scriptdir/Annotations/ANNOVAR' && perl annotate_variation.pl -buildver hg19 -downdb -webfrom annovar 'esp6500siv2_aa' humandb/ ");
system("cd '$scriptdir/Annotations/ANNOVAR' && perl annotate_variation.pl -buildver hg19 -downdb -webfrom annovar 'esp6500siv2_all' humandb/ ");
system("cd '$scriptdir/Annotations/ANNOVAR' && perl annotate_variation.pl -buildver hg19 -downdb -webfrom annovar 'esp6500siv2_ea' humandb/ ");

## force the annotations to update.
#system("cd '$scriptdir/Annotations && perl Annotate.pl");
system("echo 1 > $scriptdir/Annotations/update.txt");

## send email to admins
# notify they need to change 
#   - install php5-curl
#   - allow .htaccess overrides for api
#   - set server query_other_jobs = TRUE   : in torque qmgr.
open OUT, ">/tmp/vdb_update.email";
print OUT "from:geert.vandeweyer\@uantwerpen.be\n";
print OUT "subject:VariantDB update: Admin action required\n";
print OUT "\n";
print OUT "Dear VariantDB administrator,\r\n";
print OUT "\r\n";
print OUT "Your VariantDB installation was updated, but additional action is required to activate the new api features. Until these actions are taken, it is recommended to keep VariantDB offline (it's current status is ONLINE).\r\n\r\n";
print OUT "The following actions are needed: \r\n";
print OUT " - Install php5-curl (sudo apt-get install php5-curl ; on ubuntu)\r\n";
print OUT " - Allow .htaccess overrides for api. Example snippet for /etc/apache2/httpd.conf: \r\n";
print OUT "     <Directory \"/var/www/variantdb/api\">\r\n";
print OUT "        AllowOverride All\r\n";
print OUT "        Order allow,deny\r\n";
print OUT "        Allow from all\r\n";
print OUT "     </Directory>\r\n";
print OUT " - Enable apache Rewrite.\r\r";
print OUT "     - sudo a2enmod rewrite\r\n";
print OUT "     - sudo /etc/init.d/apache2 restart\r\n";
print OUT " - Double check permissions (writable by scriptuser + www-data) on the following files\r\n";
print OUT "     - $scriptdir/api/query_results/  (the folder)\r\n";
print OUT "     - $scriptdir/api/query_results/.job_queue\r\n";
print OUT " - if using HPC, do 'set server query_other_jobs = TRUE' using qmgr, to allow www-data to query the scriptuser jobs.\r\n";
print OUT " - if using HPC and memcached. make sure the memcached server(s) is(are) accessible from the compute nodes.\r\n";
print OUT "\r\n";
print OUT "That's it. All should be working now.\r\n";
print OUT "\r\n";
print OUT "Geert.";
close OUT;

#########################
## Connect to database ##
######################### 
print "Connecting to database\n";

# get current genome build/database to update
my $dbhgb = DBI->connect("dbi:mysql:NGS-Variants-Admin:$host",$userid,$userpass);
my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
$gbsth->execute();
my @row = $gbsth->fetchrow_array();
$gbsth->finish();
$db = "NGS-Variants".$row[0];
$newbuild = $row[1];
$ucscbuild = $row[0];
$ucscbuild =~ s/^-//;
print "\t=> Using current build database (build ".$row[1].")\n";
$dbh = DBI->connect("dbi:mysql:$db:$host",$userid,$userpass) or die("Could not connect to database with credentials provided in $file");
$dbh->{mysql_auto_reconnect} = 1;
$dbh->{PrintError} = 0;
$dbh->{RaiseError} = 0;

$sth = $dbh->prepare("SELECT email FROM `Users` WHERE level = 3");
$sth->execute();
while(my @row = $sth->fetchrow_array()) {
	system("sendmail $row[0] < /tmp/vdb_update.email");
}
system("rm /tmp/vdb_update.email");

