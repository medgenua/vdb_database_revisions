#!/usr/bin/perl

################
# LOAD MODULES #
################
use DBI;
use Getopt::Std;

my $file = $ARGV[0];

if ($file eq '') {
	# check default cred file
	if (-e '../.Credentials/.credentials') {
		$file = '../.Credentials/.credentials';
	}
	else {
		print "Path to credentials file not provided.\n";
		print " USAGE: perl ApplyUpdates.pl /path/to/.credentials.txt\n";
		exit();
	}
}

# argument to skip some steps if performing initial installation.
$install = 'regular';
if (defined($ARGV[1]) && lc($ARGV[1]) eq 'install') {
	$install = 'install';
	print "In installation mode. Will skip several tasks.\n";
	sleep 3;
}
# read in config file
my %config;
open CONFIG, $file or die "can't open $file: $!";
while (<CONFIG>) {
    chomp;
    s/#.*//; # Remove comments
    s/^\s+//; # Remove opening whitespace
    s/\s+$//;  # Remove closing whitespace
    next unless length;
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
}
close CONFIG;
$userid = $config{'DBUSER'};
$userpass = $config{'DBPASS'};
$host = $config{'DBHOST'};
$scriptdir = $config{'SCRIPTDIR'};
if ($config{'DBSUFFIX'} != '') {
    $db_suffix = "_".$config{'DBSUFFIX'};
}
else {
    $db_suffix = '';
}
# disable output buffer
$|++;

#########################
## Connect to database ##
######################### 
print "Connecting to database\n";

# get current genome build/database to update
my $dbhgb = DBI->connect("dbi:mysql:NGS-Variants-Admin$db_suffix:$host",$userid,$userpass);
my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
$gbsth->execute();
my @row = $gbsth->fetchrow_array();
$gbsth->finish();
$db = "NGS-Variants".$row[0].$db_suffix;
$newbuild = $row[1];
$ucscbuild = $row[0];
$ucscbuild =~ s/^-//;
print "\t=> Using current build database (build ".$row[1].")\n";
$mysql = DBI->connect("dbi:mysql:$db:$host",$userid,$userpass) or die("Could not connect to database with credentials provided in $file");
$mysql->{mysql_auto_reconnect} = 1;
$mysql->{PrintError} = 0;
$mysql->{RaiseError} = 0;

# get current revision from GenomicBuilds database
my $sth = $mysql->prepare("SELECT Revision FROM `NGS-Variants-Admin$db_suffix`.`DatabaseRevision` ORDER BY Revision DESC LIMIT 1");
my $nrr = $sth->execute();
# if no value in the table, set current rev to 0.
my $currentrev = 0;
if ($nrr > 0) {
	($currentrev) = $sth->fetchrow_array();
}
print "Starting update process at Revision : $currentrev\n\n";
# read in .sql names, starting from current revision + 1
$rev = $currentrev + 1;
my $first = 1;
my $currentsitestatus = '';
my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)=localtime(time);
my $date = ($year + 1900).'-'.($mon + 1).'-'.$mday;

#############################
## REMOVE PREVIOUS BACKUPS ##
#############################
if (-d 'backup') {
	print "\n";
	print " #######################################\n";
	print " # Removing previous database backup ! #\n";
	print " # Hit Ctrl-C to abort !               #\n";	
	print " #######################################\n";
	print "\n";
	sleep 10;
	system("rm -Rf backup");
}	
system("mkdir backup");

## PROCESS
my %dumped = ();
my %created = ();
while (-e "$rev.sql") {
	if ($first == 1) {
		# set platform to update
		print "Restricting access to web-interface\n";
		my $stat = $mysql->prepare("SELECT status FROM `NGS-Variants-Admin$db_suffix`.`SiteStatus`");
		$stat->execute();
		($currentsitestatus) = $stat->fetchrow_array();
		$stat->finish();
		$mysql->do("UPDATE `NGS-Variants-Admin$db_suffix`.`SiteStatus` SET status = 'Construction' WHERE 1");
		$first = 0;
	}
	if (-e "$rev.tables" && $install ne 'install') {
		# dump db table for backup
		print "Creating Backup of affected tables before updating (location: backup/)\n";
		open IN, "$rev.tables";
		while (<IN>) {
			chomp($_);
			if ($_ eq '') {
				next;
			}
			my ($dumpdb,$dumptable) = split(/;/,$_);
			if ($dumped{$dumpdb}{$dumptable} == 1) {
				## already dumped, skip
				next;
			}
			print "  => dumping $dumptable from $dumpdb\n";
			$dumped{$dumpdb}{$dumptable} = 1;
			system("mysqldump --user=$userid --password=$userpass --host=$host --quote-names --opt --max_allowed_packet=1G $dumpdb $dumptable > backup/$dumpdb--$dumptable.sql");	

		}
		close IN;
		## check for mydumper/myloader
	}
	print "Applying updates in $rev.sql\n";
	open IN, "$rev.sql";
	while (<IN>) {
		chomp($_);
		if ($_ ne '') {
		  if ($_ =~ m/CREATE TABLE `(NGS-Variants-\S+)`\.`(\S+)`\s/i) {
			$created{$1}{$2} = 1;
		  }
		  $mysql->do($_);
		  if ($DBI::err) {
			print " #########################\n";
			print " # ERROR : UPDATE FAILED #\n";
			print " #########################\n";
			print "Error while applying update. Database will be reverted to original state (Revision $currentrev).\nError: $DBI::errstr\n";
			# revert !
			print "Restoring database: \n";
			#my $dumper = `mydumper --version`;
			#my $loader = `myloader --version`;
			#if ($dumper =~ m/built against/ && $loader =~ m/built against/) {
			#	print "Using mydumper/myloader for database backup.\n";
			#	system("myloader -u $userid -p $userpass -h $host -d backup -t 6 -v 3 -o -q 5000");
			#}
			#else {
			foreach my $loaddb (keys(%dumped)) {
				foreach my $loadtable (keys(%{$dumped{$loaddb}})) {
					print "  - $loaddb - $loadtable\n";
					system("mysql --user=$userid --password=$userpass --host=$host --max_allowed_packet=1G $loaddb < backup/$loaddb--$loadtable.sql");
					$mysql->do("UPDATE `NGS-Variants-Admin$db_suffix`.`memcache_idx` SET `Table_IDX` = `Table_IDX` + 1 WHERE `Table_Name` = '$loadtable'");

				}
			}
			foreach my $cdb (keys(%created)) {
				foreach my $ct (keys(%{$created{$cdb}})) {
					if ($cdb ne '' && $ct ne '') {
						print "Dropping table : $cdb:$ct\n";
						$mysql->do("DROP TABLE `$cdb`.`$ct`");
					}
				}

			}
			print "Done\n";
			$mysql->do("UPDATE `NGS-Variants-Admin$db_suffix`.`SiteStatus` SET status = '$currentsitestatus' WHERE 1");
			# trigger validation
			open LCK, ">$scriptdir/Query_Results/.validator.lck";
			flock(LCK,2);
			open Q, ">$scriptdir/Query_Results/.validator.queue";
			print Q "Anno\n";
			close Q;
			close LCK;
			exit;
		  }
		}
	}
	close IN;
	## check for script to execute.
	if (-e "$rev.pl") {
		print "Applying update in script $rev.pl\n";
		if (system("perl '$rev.pl' '$file' '$install'") != 0) {
			print " #########################\n";
			print " # ERROR : UPDATE FAILED #\n";
			print " #########################\n";
			print "Error while applying update in $rev.pl. Database will be reverted to original state (Revision $currentrev).\n";
			# revert !
			print "Restoring database: \n";
			#my $dumper = `mydumper --version`;
			#my $loader = `myloader --version`;
			#if ($dumper =~ m/built against/ && $loader =~ m/built against/) {
			#	print "Using mydumper/myloader for database backup.\n";
			#	system("myloader -u $userid -p $userpass -h $host -d backup -t 6 -v 3 -o -q 5000");
			#}
			#else {
			foreach my $loaddb (keys(%dumped)) {
				foreach my $loadtable (keys(%{$dumped{$loaddb}})) {
					system("mysql --user=$userid --password=$userpass --host=$host --max_allowed_packet=1G $loaddb < backup/$loaddb--$loadtable.sql");
					$mysql->do("UPDATE `NGS-Variants-Admin$db_suffix`.`memcache_idx` SET `Table_IDX` = `Table_IDX` + 1 WHERE `Table_Name` = '$loadtable'");
				}
			}
			foreach my $cdb (keys(%created)) {
				foreach my $ct (keys(%{$created{$cdb}})) {
					$mysql->do("DROP TABLE `$cdb`.`$ct`");
				}

			}
			print "Done\n";
			$mysql->do("UPDATE `NGS-Variants-Admin$db_suffix`.`SiteStatus` SET status = '$currentsitestatus' WHERE 1");
			# trigger validation
			open LCK, ">$scriptdir/Query_Results/.validator.lck";
			flock(LCK,2);
			open Q, ">$scriptdir/Query_Results/.validator.queue";
			print Q "Anno\n";
			close Q;
			close LCK;

			exit;

		}
	}
	$rev++;
}
$rev--;
if ($rev == $currentrev) {
	print "No updates available. Database not changed\n";
	exit();
}
else {
	print "Restoring access to Web-Interface\n";
	$mysql->do("UPDATE `NGS-Variants-Admin$db_suffix`.`SiteStatus` SET status = '$currentsitestatus' WHERE 1");
	print "Database Updated successfully to revision : $rev\n";
	print "The original state was stored in backup/\n";
	print "It was left there, in case something did go wrong after all\n";
	foreach my $loaddb (keys(%dumped)) {
		foreach my $loadtable (keys(%{$dumped{$loaddb}})) {
			$mysql->do("UPDATE `NGS-Variants-Admin$db_suffix`.`memcache_idx` SET `Table_IDX` = `Table_IDX` + 1 WHERE `Table_Name` = '$loadtable'");
		}
	}

	
}

# store time (auto) and revision in database.
$mysql->do("INSERT INTO `NGS-Variants-Admin$db_suffix`.`DatabaseRevision` (Revision) VALUES ('$rev')");
# trigger validation
open LCK, ">$scriptdir/Query_Results/.validator.lck";
flock(LCK,2);
open Q, ">$scriptdir/Query_Results/.validator.queue";
print Q "Anno\n";
close Q;
close LCK;

