#!/usr/bin/perl

use DBI;
use Getopt::Std;

my $file = $ARGV[0];
if ($file eq '') {

	# check default cred file
	if (-e '../.Credentials/.credentials') {
		$file = '../.Credentials/.credentials';
	}
	else{
		print "Path to credentials file not provided.\n";
		print " USAGE: perl ApplyUpdates.pl /path/to/.credentials.txt\n";
		exit();
	}
}
# read in config file
my %config;
open CONFIG, $file or die "can't open $file: $!";
while (<CONFIG>) {
    chomp;
    s/#.*//; # Remove comments
    s/^\s+//; # Remove opening whitespace
    s/\s+$//;  # Remove closing whitespace
    next unless length;
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
}
close CONFIG;
$admin = $config{'ADMINMAIL'};
# disable output buffer
$|++;


# argument to skip some steps if performing initial installation.
$install = 0;
if (defined($ARGV[1]) && $ARGV[1] eq 'install') {
	$install = 1;
}




###### AIM : 
###### notify system admin to install some CPAN modules. 

my $from = 'geert.vandeweyer@uantwerpen.be';
my $to = $admin;
my $subject = 'VariantDB : Action Required';
my $content  = "Dear Administrator,\r\n\r\n";
   $content .= "Two additional perl modules are required for some novel functionality in VariantDB. These must be manually installed on the web-server:\r\n";
   $content .= "  - MIME::Types  (a dependency)\r\n";
   $content .= "  - MIME::Lite   (send mails with attachments)\r\n\r\n";
   $content .= "These modules are used to inform you, as an admin, of errors and problems after platform updates. To install them, use the following commands:\r\n\r\n";
   $content .= "  sudo perl getCpanModules.pl 'MIME::Types'\r\n";
   $content .= "  sudo perl getCpanModules.pl 'MIME::Lite'\r\n\r\n";
   $content .= "This script is located in the original installation folder of VariantDB, or can be obtained using:\r\n";
   $content .= "  hg clone https://bitbucket.org/medgenua/vdb_installer/\r\n\r\n";
   $content .= "Best regards,\r\nGeert Vandeweyer\r\n";
		
open OUT, ">/tmp/admin.mail.txt";
print OUT "BCC:geert.vandeweyer\@uantwerpen.be\n";
print OUT "To: $admin\n";
print OUT "subject: $subject\n";
print OUT "from: $from\n\n";
print OUT $content;
close OUT;

system("sendmail -t < /tmp/admin.mail.txt");
system("rm /tmp/admin.mail.txt");
