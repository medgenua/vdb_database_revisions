#!/usr/bin/perl

use DBI;
use Getopt::Std;

# not needed anymore. 
exit;
my $file = $ARGV[0];
if ($file eq '') {

        # check default cred file
        if (-e '../.Credentials/.credentials') {
                $file = '../.Credentials/.credentials';
        }
        else{
                print "Path to credentials file not provided.\n";
                print " USAGE: perl ApplyUpdates.pl /path/to/.credentials.txt\n";
                exit();
        }
}
# read in config file
my %config;
open CONFIG, $file or die "can't open $file: $!";
while (<CONFIG>) {
    chomp;
    s/#.*//; # Remove comments
    s/^\s+//; # Remove opening whitespace
    s/\s+$//;  # Remove closing whitespace
    next unless length;
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
}
close CONFIG;
$userid = $config{'DBUSER'};
$userpass = $config{'DBPASS'};
$host = $config{'DBHOST'};
$scriptdir = $config{'SCRIPTDIR'};
# disable output buffer
$|++;


## FIX annovar splice site annotation issues.

print "Connecting to database\n";

# get current genome build/database to update
my $dbhgb = DBI->connect("dbi:mysql:NGS-Variants-Admin:$host",$userid,$userpass);
my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
$gbsth->execute();
my @row = $gbsth->fetchrow_array();
$gbsth->finish();
$db = "NGS-Variants".$row[0];
$newbuild = $row[1];
$ucscbuild = $row[0];
$ucscbuild =~ s/^-//;
print "\t=> Using current build database (build ".$row[1].")\n";
$dbh = DBI->connect("dbi:mysql:$db:$host",$userid,$userpass) or die("Could not connect to database with credentials provided in $file");
$dbh->{mysql_auto_reconnect} = 1;
$dbh->{PrintError} = 0;
$dbh->{RaiseError} = 0;

my %chromhash ;
%chromhash = (); 
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "X" } = 23;
$chromhash{ "Y" } = 24; 
$chromhash{ "M" } = 25;
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y";
$chromhash{ "25" } = "M";
my %caphash = ( 'refgene','refGene', 
		'knowngene','knownGene',
		'ensgene','ensGene');



# handle all three gene annotations
my @tables = qw/refgene ensgene knowngene/;
foreach my $anno (@tables) {
	my $capanno = $caphash{$anno};

	my $table = "Variants_x_ANNOVAR_$anno";
	## 1. Get list of affected variants.
	my $affected_rows = $dbh->selectall_arrayref("SELECT aid, vid,GeneSymbol FROM `$table` WHERE GeneSymbol LIKE '\%(\%:%)\%'");
	my $vids = [];
	foreach my $row (@$affected_rows) {
		push(@$vids,$row->[1]);
		#2. delete the incorrect annotation.
		$dbh->do("DELETE FROM `$table` WHERE aid = '".$row->[0]."' AND vid = '".$row->[1]."'");
	}
	$affected_rows = {};
	# 3. Create input list for ANNOVAR.
	my $vid_str = join(",",@$vids);
	my $query = "SELECT v.chr, v.start, v.stop, v.RefAllele, v.AltAllele, v.id AS VariantID FROM `Variants` v WHERE v.id IN ($vid_str)";
	my $variants = $dbh->selectall_arrayref($query);
	# goto next annotation if nothing to do.
	if (scalar(@$variants) == 0) {
		print "nothing to do for $anno\n";
		next;
	}
	my %variant_strings = ();
	open OUT, ">$scriptdir/Annotations/ANNOVAR/Input_Files/$anno.fix.list.txt";
	foreach my $result (@$variants)  {
		$result->[0] = $chromhash{$result->[0]};
		$variant_strings{$result->[5]} = "chr".$result->[0].":".$result->[1]." ".$result->[3]."/".$result->[4];

		if ($result->[4] =~ m/N/) {
			print "WARNING: $result->[4] has -N-. This is replaced by -A- for annotation\n";
			$result->[4] =~ s/N/A/g;
		}
		print OUT $result->[0]."\t".$result->[1]."\t".$result->[2]."\t".$result->[3]."\t".$result->[4]."\t".$result->[5]."\n";
	}
	close OUT;
	# 4. annotate 
	system("cd $scriptdir/Annotations/ANNOVAR/ ; perl annotate_variation.pl --memtotal 2000000 --buildver '$ucscbuild' --dbtype $anno --outfile Output_Files/$anno.fix Input_Files/$anno.fix.list.txt humandb/");
	## 5. store to db
	## load codings
	my %codes = ();
	my $sth = $dbh->prepare("SELECT id, Item_Value FROM `Value_Codes` WHERE Table_x_Column = 'Variants_x_ANNOVAR_$anno"."_GeneLocation'");
	$sth->execute();
	while (my @r = $sth->fetchrow_array()) {
		$codes{"GeneLocation_".$r[1]} = $r[0];
	}
	$sth->finish();
	my $sth = $dbh->prepare("SELECT id, Item_Value FROM `Value_Codes` WHERE Table_x_Column = 'Variants_x_ANNOVAR_$anno"."_VariantType'");
	$sth->execute();
	while (my @r = $sth->fetchrow_array()) {
		$codes{"VariantType_".$r[1]} = $r[0];

	}
	$sth->finish();
	my %hash = ();
	my %lines = ();
	$sth = $dbh->prepare("INSERT INTO `Variants_x_ANNOVAR_$anno` (vid, GeneSymbol, Transcript, Exon, GeneLocation, VariantType, CPointNT,CPointAA) VALUES (?,?,?,?,?,?,?,?)");
	## first : anno.variant_function
	open IN, "$scriptdir/Annotations/ANNOVAR/Output_Files/$anno.fix.variant_function" or die("Could not open $scriptdir/Annotations/ANNOVAR/Output_Files/$anno.fix.variant_function \n");
	my %newvids = ();
	while (<IN>) {
		chomp($_);
		my @p = split(/\t/,$_);
		my $gloc = $p[0];
		if (!defined($codes{"GeneLocation_".$p[0]})) {
			$dbh->do("INSERT INTO `Value_Codes` (Table_x_Column,Item_Value) VALUES ('Variants_x_ANNOVAR_$anno"."_GeneLocation','$p[0]')");
			$codes{"GeneLocation_".$p[0]} = $dbh->last_insert_id(undef,undef,undef,undef);
		}
		$p[0] = $codes{"GeneLocation_".$p[0]};	 
		$hash{$p[-1]}{'loc'} = $p[0];
		$hash{$p[-1]}{'symbol'} = $p[1];
		
		## splice variants are coded here, not in exonic:
		if ($p[1] =~ m/(.*)\((.*)\)/) {
			if ($gloc =~ m/splicing/i){
				
				$newvids{$p[-1]} = 1;
			}
			$hash{$p[-1]}{'symbol'} = $1;
			my @t = split(/,/,$2);
			if (!defined($codes{"VariantType_".$gloc})) {
				$dbh->do("INSERT INTO `Value_Codes` (Table_x_Column,Item_Value) VALUES ('Variants_x_ANNOVAR_$anno"."_VariantType','$gloc')");
				$codes{"VariantType_".$gloc} = $dbh->last_insert_id(undef,undef,undef,undef);
			}
			my $vt = $codes{"VariantType_".$gloc};
			foreach (@t) {
				my @items = split(/:/,$_);
				## execute variantID, symbol, transcript, Exon, geneloc, variantType, cpointNT,cpointAA
				## replace empty by .
				for (my $i = 0; $i <= 2; $i++) {
					if ($items[$i] eq '') {
						$items[$i] = '.';
					}
				}
				$items[1] =~ s/exon//;
				$sth->execute($p[-1],$hash{$p[-1]}{'symbol'},$items[0],$items[1],$hash{$p[-1]}{'loc'},$vt,$items[2],'.') ;
				## get insertID and store by linenr
				my $aid = $dbh->last_insert_id(undef,undef,undef,undef);	
				$lines{$p[0]}{'a'} = $aid;
				$lines{$p[0]}{'v'} = $p[-1];
			}
			delete($hash{$p[-1]});
		}
		else {
			# only update splice variants. other types would be present as doubles.
			next;
		}
	}
	close IN;
	$sth->finish();
	## gene panels.
	if ($anno eq 'refgene') {
		# get current annotations for the vids of interest.
		#my $current_rows = $dbh->selectall_arrayref("SELECT aid,vid,gpid,gene FROM `Variants_x_GenePanels` WHERE vid IN (".join(",",keys(%newvids)).")");
		#my %current = ();
		#foreach(@$current_rows) {
		#	$current{$_->[1]."-".$_->[2]."-".$_->[3]} = 1;
		#}
		# annotate.
		system("cd $scriptdir/Annotations/GenePanels ; perl LoadVariants.pl");
		# get new annotations.
		my $new_rows = $dbh->selectall_arrayref("SELECT aid,vid,gpid,gene FROM `Variants_x_GenePanels` WHERE vid IN (".join(",",keys(%newvids)).")");
		my %new = ();
		foreach(@$new_rows) {
		#	if (!defined($current{$_->[1]."-".$_->[2]."-".$_->[3]})) {
				$new{$_->[1]}{$_->[2]} = $_->[3];
		#	}
		}
		$new_rows = [];
		# compose mails.
		my %mails = ();
		foreach my $vid (keys(%new)) {
			foreach my $gpid (keys(%{$new{$vid}})) {
				my $users = $dbh->selectall_arrayref("SELECT u.id,u.email, u.LastName, u.FirstName FROM `GenePanels_x_Users` gu JOIN `Users` u ON gu.uid = u.id WHERE gu.gpid = $gpid");
				foreach my $user (@$users) {
					if (!defined($mails{$user->[0]})) {
						$mails{$user->[0]}{'email'} = $user->[1];
						$mails{$user->[0]}{'name'} = $user->[3]. " ".$user->[2];
						$mails{$user->[0]}{'text'} = '';
					}
					# get samples
					$query = "SELECT s.Name AS sname, p.Name AS pname FROM `Samples` s JOIN `Projects_x_Samples` ps JOIN `Projects_x_Users` pu JOIN `Projects` p JOIN `Variants_x_Samples` vs ON pu.pid = p.id AND s.id = ps.sid AND p.id = ps.pid AND s.id = vs.sid WHERE vs.vid = '$vid' AND pu.uid = '".$user->[0]."'";
					my $samples = $dbh->selectall_arrayref($query);
					if (scalar(@$samples) > 0) {
						$mails{$user->[0]}{'text'} .= "\r\nVariant: $variant_strings{$vid} :\r\n - GENE: $new{$vid}{$gpid}\r\n";
						foreach my $row (@$samples) {
							$mails{$user->[0]}{'text'} .= " - $row->[0] : project $row->[1]\r\n";
						}
					}
				}

			}
		}
		## send mails.
		foreach my $muid (keys(%mails)) {
			if ($mails{$muid}{'text'} ne '') {
				open OUT, ">/tmp/mail.$muid.txt";
				print OUT "to: $mails{$muid}{'email'}\n";
				print OUT "cc: $adminemail\n";
				print OUT "from: $adminemail\n";
				print OUT "subject: VariantDB bug fixed\n\n";
				print OUT "Dear $mails{$muid}{'name'},\r\n";
				print OUT "\r\n";
				print OUT "An annotation glitch within VariantDB was fixed, which affected gene assignment of splice variants. This might have affected filtering for splice variants within a user-defind genepanel. Below, you will find a list of affected variants, and the samples they are present in. It is advised to re-evaluate these variants for their relevance.\r\n";
				print OUT "\r\nKind Regards,\r\nGeert\r\n\r\nRESULTS:\r\n";
				print OUT $mails{$muid}{'text'};
				close OUT;
				system("sendmail -t < '/tmp/mail.$muid.txt'");
			}
		}
	}
	$dbh->do("UPDATE `NGS-Variants-Admin`.`memcache_idx` SET `Table_IDX = `Table_IDX` + 1 WHERE `Table_Name` = 'Variants_x_ANNOVAR_$anno'");
	
	#
	## clean up.
	#system("rm -f Input_Files/$anno.list.txt");
	#system("rm -f Output_Files/$anno.Coding.change.txt");
	#system("rm -f Output_Files/$anno.exonic_variant_function");
	#system("rm -f Output_Files/$anno.variant_function");
	system("rm -f Output_Files/$anno.fix.*");
	$dbh->disconnect();
	#if ($usemc == 1) {
	#	clearMemcache("Variants_x_ANNOVAR_$anno");
	#}

}

