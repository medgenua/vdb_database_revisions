#!/usr/bin/perl

# GOAL : 
#	- rewrite saved filters to correctly use the Summary tables for "In_All_Control_Samples"

use DBI;

my $file = $ARGV[0];
if ($file eq '') {

	# check default cred file
	if (-e '../.Credentials/.credentials') {
		$file = '../.Credentials/.credentials';
	}
	else{
		print "Path to credentials file not provided.\n";
		print " USAGE: perl ApplyUpdates.pl /path/to/.credentials.txt\n";
		exit();
	}
}
# read in config file
my %config;
open CONFIG, $file or die "can't open $file: $!";
while (<CONFIG>) {
    chomp;
    s/#.*//; # Remove comments
    s/^\s+//; # Remove opening whitespace
    s/\s+$//;  # Remove closing whitespace
    next unless length;
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
}
close CONFIG;
$userid = $config{'DBUSER'};
$userpass = $config{'DBPASS'};
$host = $config{'DBHOST'};
$datadir = $config{'DATADIR'};
$scriptpass = $config{'SCRIPTPASS'};
$scriptuser = $config{'SCRIPTUSER'};
$scriptdir = $config{'SCRIPTDIR'};

print "Connecting to database\n";

# get current genome build/database to update
my $dbhgb = DBI->connect("dbi:mysql:NGS-Variants-Admin:$host",$userid,$userpass);
my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
$gbsth->execute();
my @row = $gbsth->fetchrow_array();
$gbsth->finish();
$db = "NGS-Variants".$row[0];
$newbuild = $row[1];
$ucscbuild = $row[0];
$ucscbuild =~ s/^-//;
print "\t=> Using current build database (build ".$row[1].")\n";
$dbh = DBI->connect("dbi:mysql:$db:$host",$userid,$userpass) or die("Could not connect to database with credentials provided in $file");
$dbh->{mysql_auto_reconnect} = 1;
$dbh->{PrintError} = 0;
$dbh->{RaiseError} = 0;

## get filters to edit : match all with 'Samples' (male/female/control/all/abs/rel/...)
my $sth = $dbh->prepare("SELECT fid,uid,FilterName,FilterSettings,FilterTree FROM `Users_x_FilterSettings` WHERE FilterSettings LIKE '%Samples%'");

$sth->execute();
my $rc = $sth->fetchall_arrayref();

# replacement values. (can be extended in future replacement scripts)
my %replace = ();
%{$replace{'In_All_Control_Samples'}} = (	'0,1,2' => '`Variants_x_Users_Summary`.ConHomRef + `Variants_x_Users_Summary`.ConHet + `Variants_x_Users_Summary`.ConHomAlt',
						'1,2' => '`Variants_x_Users_Summary`.ConHet + `Variants_x_Users_Summary`.ConHomAlt',
						'1' => '`Variants_x_Users_Summary`.ConHet',
						'2' => '`Variants_x_Users_Summary`.ConHomAlt'
				     );
# prepare the actual update query.
my $update = $dbh->prepare("UPDATE `Users_x_FilterSettings` SET FilterSettings = ? WHERE fid = ?");


foreach my $r (@$rc) {
	my $do_update = 0;	
	# settings to hash.
	my %settings = ();
	my %fidxs = ();
	foreach my $item (split(/@@@/,$r->[3])) {
		my ($k,$v) = split(/\|\|\|/,$item);
		if ($k =~ m/category(\d+)/) {
			$fidxs{$1} = 1;
		}
		$settings{$k} = $v;
	}
	# look for the items we need to update
	foreach my $idx (keys(%fidxs)) {
		# it's all in 'occurence' category.
		next if ($settings{"category$idx"} ne 'Occurence');
		# case : In_Control_Samples filter (was old syntax, not working anymore. let's correct this as well)	
		if ($settings{"param$idx"} eq 'In_Control_Samples') {
			$settings{"param$idx"} = 'In_All_Control_Samples';
		}
		# all-control-samples : only update if the old syntax is present : eg (1,2)
		if ($settings{"param$idx"} eq 'In_All_Control_Samples' && defined($replace{'In_All_Control_Samples'}{$settings{"value$idx"}})) {
			my $v = $settings{"value$idx"};
			my $repl = $replace{'In_All_Control_Samples'}{$v};
			$settings{"value$idx"} = $repl;
			$do_update = 1;
			next;
		}
	}
	next if ($do_update == 0);
	my $new_settings = '';
	# recompose the settings
	foreach my $k ( keys(%settings)) {
			my $v = $settings{$k};
			$new_settings .= "$k|||$v@@@";
		
	}
	# update the database entry.
	$update->execute($new_settings,$r->[0]) or die("Failed to update $r->[0]");
}


