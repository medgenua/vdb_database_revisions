DROP TABLE IF EXISTS `NGS-Variants-hg19`.`Variants_x_SIFT`;
CREATE TABLE  `NGS-Variants-hg19`.`Variants_x_SIFT` (`aid` INT( 11 ) NOT NULL AUTO_INCREMENT , `vid` INT( 11 ) NOT NULL , `Score` FLOAT( 4, 3 ) NOT NULL , `Effect` VARCHAR( 35 ) NOT NULL , `Updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,PRIMARY KEY (  `aid` ) ,KEY  `vid` (  `vid` ) ,KEY  `Score` (  `Score` ) ,KEY  `Effect` (  `Effect` )) ENGINE = MYISAM DEFAULT CHARSET = latin1;
ALTER TABLE  `Variants_x_SIFT` CHANGE  `Score`  `SIFTScore` FLOAT( 4, 3 ) NOT NULL ,CHANGE  `Effect`  `SIFTEffect` VARCHAR( 35 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
ALTER TABLE  `Variants_x_SIFT` ADD  `PROVEANScore` FLOAT( 4, 3 ) NOT NULL AFTER  `SIFTEffect` ,ADD  `PROVEANEffect` VARCHAR( 35 ) NOT NULL AFTER  `PROVEANScore`;
ALTER TABLE  `Variants_x_SIFT` ADD INDEX (  `PROVEANScore` );
ALTER TABLE  `Variants_x_SIFT` ADD INDEX (  `PROVEANEffect` );
ALTER TABLE  `Variants_x_SIFT` ADD  `ensp` VARCHAR( 20 ) NOT NULL AFTER  `PROVEANEffect` ,ADD  `plen` INT( 5 ) NOT NULL AFTER  `ensp` ,ADD  `ppos` INT( 5 ) NOT NULL AFTER  `plen` ,ADD  `resref` TEXT NOT NULL AFTER  `ppos` ,ADD  `resalt` TEXT NOT NULL AFTER  `resref` ,ADD  `type` VARCHAR( 30 ) NOT NULL AFTER  `resalt` ,ADD  `grantham` INT NOT NULL AFTER  `type`;
