#!/usr/bin/perl

use DBI;
use Getopt::Std;

my $file = $ARGV[0];
if ($file eq '') {

        # check default cred file
        if (-e '../.Credentials/.credentials') {
                $file = '../.Credentials/.credentials';
        }
        else{
                print "Path to credentials file not provided.\n";
                print " USAGE: perl ApplyUpdates.pl /path/to/.credentials.txt\n";
                exit();
        }
}
# read in config file
my %config;
open CONFIG, $file or die "can't open $file: $!";
while (<CONFIG>) {
    chomp;
    s/#.*//; # Remove comments
    s/^\s+//; # Remove opening whitespace
    s/\s+$//;  # Remove closing whitespace
    next unless length;
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
}
close CONFIG;
$userid = $config{'DBUSER'};
$userpass = $config{'DBPASS'};
$host = $config{'DBHOST'};
$scriptdir = $config{'SCRIPTDIR'};
# disable output buffer
$|++;

# argument to skip some steps if performing initial installation.
$install = 0;
if (defined($ARGV[1]) && $ARGV[1] eq 'install') {
	$install = 1;
	# annotation jobs. skip
	exit;
}



# updata clinvar database
print "Updating ClinVar database\n";
if (-e "$scriptdir/Annotations/ClinVar/ClinVarFullRelease_Loaded.xml") {
	print "Removing current ClinVar datafile\n";
	unlink("$scriptdir/Annotations/ClinVar/ClinVarFullRelease_Loaded.xml");
}

system("cd '$scriptdir/Annotations/ClinVar' && perl UpdateClinVar.pl ");
print "Re-Annotating variants with ClinVar information.\n";
system("cd '$scriptdir/Annotations/ClinVar' && perl LoadVariants.pl");

# load snp138 data
print "Downloading db-snp138 from ANNOVAR website\n";
system("cd '$scriptdir/Annotations/ANNOVAR' && perl annotate_variation.pl -buildver hg19 -downdb -webfrom annovar 'snp138' humandb/ ");
print "Annotating Variants with db-snp138\n";
system("cd '$scriptdir/Annotations/ANNOVAR' && perl LoadVariants.pl -a snp138");

exit;
