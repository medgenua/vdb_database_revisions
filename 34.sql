ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_Samples` ADD  `DeltaPL` INT NOT NULL DEFAULT  '0' COMMENT  'Difference in PL between Call (zero) and lowest other value',ADD INDEX (  `DeltaPL` );
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_Samples` ADD INDEX (  `VQSLOD` )
