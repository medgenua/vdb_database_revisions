ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_Samples` ADD INDEX (  `inheritance` );
ALTER TABLE  `NGS-Variants-hg19`.`Variants_x_Samples` CHANGE  `validation`  `validation` VARCHAR( 25 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT  '-' COMMENT  'validation method, filled from limited list';
UPDATE `NGS-Variants-hg19`.`Variants_x_Samples` SET validation = '-' WHERE validation = '';
